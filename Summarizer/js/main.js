
function submit() {
  if (document.getElementById('result')) {
    removeById('result');
  }
  addLoader();
  var url = document.getElementById('URL').value;
  var request = new XMLHttpRequest();
  request.open("POST", "php/getHTML.php", true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.onreadystatechange = function() {
    if (request.readyState === 4) {
      if (request.status === 200) {
        document.body.className = 'ok';
        var content = request.responseText;
        //Do your thing here
        parse(content);
      }
      else {
        removeById('loader');
        addText("Response error");
      }
    }
  }
  request.send('url=' + url);
}

function parse(content) {
  var final = "";
  parser = new DOMParser();
  doc = parser.parseFromString(content, "text/html");
  area = doc.getElementsByClassName('postArticle-content')[0];
  var arr = area.getElementsByTagName('p');
  for(var i = 0; i < arr.length; i++){
    final += arr[i].textContent + "\n";
  }
  // Get the title
  var title = doc.getElementsByTagName("h1")[0].textContent;
  // Send to the API to communicate
  API(final, title);
}

function API(fullText, title) {
  //Take the number of sentences
  var e = document.getElementById("dlist");
  var senNum = e.options[e.selectedIndex].value;

  //Make the request to API
  var request = new XMLHttpRequest();
  request.open("POST", "php/API.php", true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //request is received
  request.onreadystatechange = function() {
    if (request.readyState === 4) {
      if (request.status === 200) {
        document.body.className = 'ok';
        var response = request.responseText;
        //Do your thing here with the response
        removeById('loader');
        addText(response);
      }
      else {
        removeById('loader');
        addText("Response error");
      }
    }
  }
  //send here
  request.send('fullText=' + fullText + "&title=" + title + "&sentences_number=" + senNum);
}

function addLoader() {
  var div = document.createElement('div');
  div.id = "loader";
  document.getElementById('wrapper').appendChild(div);
}

function removeById(ID) {
  var elem = document.getElementById(ID);
  elem.parentElement.removeChild(elem);
}

function addText(text) {
  var p = document.createElement('p');
  p.id = "result";
  p.innerHTML = text;
  document.getElementById('wrapper').appendChild(p);
}