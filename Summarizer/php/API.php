<?php
// get text and title
$fullText = $_POST['fullText'];
$title = $_POST['title'];
$senNum = $_POST['sentences_number'];

get_response( $fullText, $title, $senNum);

function get_response( $fullText, $title, $senNum ) {
	$url = 'https://api.aylien.com/api/v1/summarize';
	$key = '9c00b232f654b011cdfff7e3cb34163e';
	$ID = '762aa4ef';
  $options = array(
      CURLOPT_HTTPHEADER => array("X-AYLIEN-TextAPI-Application-Key: $key",
      	"X-AYLIEN-TextAPI-Application-ID: $ID",
      	"Accept: */*",
      	"accept-encoding: gzip, deflate"
      ),
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POSTFIELDS => array('text' => $fullText, 'title' => $title, 'sentences_number' => $senNum )
  );

  $ch = curl_init($url);
  curl_setopt_array($ch, $options);
  $content = curl_exec($ch);
  $err = curl_errno($ch);
  $errmsg = curl_error($ch);
  $header = curl_getinfo($ch);
  curl_close($ch);

  $header['errno'] = $err;
  $header['errmsg'] = $errmsg;
  $arr = json_decode($content);
  $final = "<h1>$title</h1>";

  foreach($arr->sentences as $sentence){
  	$final .= $sentence . '<br>';
  }
  print_r($final);
}

