#!/usr/bin/php
<?php
/* this function initialize all arguments entered by user, if no 
argument entered, -1 is assigned to arguments  
*/
function argInitialize($a)
{
  $allArg = array();
  if (isset($a["a"]))
    $author = $a["a"];
  else
    $author = -1;

  if (isset($a["d"]))
    $date = $a["d"];
  else
    $date = -1;

  if (isset($a["t"]))
    $time = $a["t"];
  else
    $time = -1;

  if (isset($a["e"]))
    $email = $a["e"];
  else
    $email = -1;

  if (isset($a["s"]))
    $seconds = $a["s"];
  else
    $seconds = -1;

  if (isset($a["m"]))
    $msg = $a["m"];
  else
    $msg = -1;

  $allArg[0] = $author;
  $allArg[1] = $date;
  $allArg[2] = $time;
  $allArg[3] = $email;
  $allArg[4] = $seconds;
  $allArg[5] = $msg;
  return $allArg;
}

/* this function takes 2 arrays as input, and returns all log into an
array
*/
function listLog($output, $history)
{
  //fill all log in an array 
  foreach ($output as $line)
  {
    //Returns the position of where the first occurrence exists relative
    //to the beginning of the subtring in string (returns 0)
    if (strpos($line, 'commit') === 0)
    {
      if (!empty($commit))
      {
        //push elements to end of array history
        array_push($history, $commit);  
        unset($commit);//destroy array commit
      }
      //insert hash in commit array 
      $commit['hash'] = substr($line, strlen('commit'));
    }

    else if (strpos($line, 'Author') === 0)
    {
      //insert author in commit array
      $commit['author'] = substr($line, strlen('Author:'));
    }

    else if (strpos($line, 'Date') === 0)
    {
      //insert date in commit array
      $commit['date'] = substr($line, strlen('Date:'));
    }

    else
    {   
      //read the message and insert it in commit array
      if (isset($commit['message']))
      {
        $commit['message'] .= $line;
      }
      else
      {
        $commit['message'] = $line;
      }
    }
  }

  if (!empty($commit)) 
  {
    array_push($history, $commit);
  }
  return $history;
}

/* this function searches for specific entry in the array, and returns 
the result for the user
*/
function search($history, $author, $date, $time, $email, $seconds, $msg)
{
  $result = array();
  $size = sizeof($history);
  $counter = 1;

  for ($i = 0; $i < $size; $i++)
  {
    $name = explode(" ", $history[$i]['author']);
    $emai = trim($name[2], "<>");
    $dat = explode(" ", $history[$i]['date']);
    $tim = explode(":", $dat[6]);
    $msgFormated = trim($history[$i]['message']);
    $dFormatedS = trim($history[$i]['date']);
    $timestamp = strtotime($history[$i]['date']);

    $dateee = date_create($history[$i]['date']);
    $dFormated = date_format($dateee,"D-m-Y");
    
    if (($name[1] == $author || $author == -1) &&
       ($msgFormated == $msg || $msg == -1) &&
       ($dFormated == $date || $date == -1) &&
       ($tim[0].":".$tim[1] == $time || $time == -1) &&
       ($emai == $email || $email == -1) &&
       ($timestamp == $seconds || $seconds == -1))
    {
      echo $counter . ":: " . $history[$i]["hash"] . " - " . $name[1] .
           " - " . $dFormatedS . " - " . $msgFormated . "\n";
      $counter++;
    }
  }
}

$a = getopt("a:d:t:e:s:m:");
$output = array();
exec("git log", $output);
$history = array();

$allArg = argInitialize($a);
$result = listLog($output, $history);
search($result, $allArg[0] , $allArg[1], $allArg[2], $allArg[3], 
       $allArg[4], $allArg[5]);

?>