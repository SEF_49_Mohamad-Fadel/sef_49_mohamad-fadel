 drop database mohakame;
create database mohakame;
use mohakame;
CREATE TABLE Claims (
    claim_id INT,
    patient_name VARCHAR(25),
    PRIMARY KEY (claim_id)
);

CREATE TABLE Defendants (
    claim_id INT,
    defendant_name VARCHAR(25)
);

CREATE TABLE LegalEvents (
    claim_id INT,
    defendant_name VARCHAR(25),
    claim_status VARCHAR(25),
    change_date DATE
);

CREATE TABLE ClaimStatusCodes (
    claim_status VARCHAR(25),
    claim_status_desc VARCHAR(25),
    claim_seq INT
);

insert into Claims values
(1,'Bassem Dghaidi'),
(2,'Omar Breidi'),
(3,'Marwan Sawwan');

insert into ClaimStatusCodes values
("AP","Awaiting review panel",1),
("OR","Panel opinion rendered",2),
("SF","Suit filed",3),
("CL","Closed",4);

insert into Defendants values
(1,"Jean Skaff"),
(1,"Elie Meouchi"),
(1,"Radwan Sameh"),
(2,"Joseph Eid"),
(2,"Paul Syoufi"),
(2,"Radwan Sameh"),
(3,"Issam Awwad");

insert into LegalEvents values
(1,'Jean Skaff','AP','2016-01-01'),
(1,'Jean Skaff','OR','2016-02-02'),
(1,'Jean Skaff','SF','2016-03-01'),
(1,'Jean Skaff','CL','2016-04-01'),
(1,'Radwan Sameh','AP','2016-01-01'),
(1,'Radwan Sameh','OR','2016-02-02'),
(1,'Radwan Sameh','SF','2016-03-01'),
(1,'Elie Meouchi','AP','2016-01-01'),
(1,'Elie Meouchi','OR','2016-02-02'),
(2,'Radwan Sameh','AP','2016-01-01'),
(2,'Radwan Sameh','OR','2016-02-01'),
(2,'Paul Syoufi','AP','2016-01-01'),
(3,'Issam Awwad','AP','2016-01-01');

use mohakame;
SELECT 
    p.claim_id, cl.patient_name, c.claim_status
FROM
    (SELECT 
        claim_id, MIN(m) AS minStatus
    FROM
        (SELECT 
        claim_id, defendant_name, COUNT(claim_status) AS m
    FROM
        LegalEvents
    GROUP BY claim_id , defendant_name) AS d
    GROUP BY d.claim_id) AS p
        JOIN
    ClaimStatusCodes AS c ON c.claim_seq = p.minStatus
        JOIN
    Claims AS cl ON cl.claim_id = p.claim_id
ORDER BY p.claim_id