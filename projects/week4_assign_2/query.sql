use mohakame;
SELECT 
    p.claim_id, cl.patient_name, c.claim_status
FROM
    (SELECT 
        claim_id, MIN(m) AS minStatus
    FROM
        (SELECT 
        claim_id, defendant_name, COUNT(claim_status) AS m
    FROM
        LegalEvents
    GROUP BY claim_id , defendant_name) AS d
    GROUP BY d.claim_id) AS p
        JOIN
    ClaimStatusCodes AS c ON c.claim_seq = p.minStatus
        JOIN
    Claims AS cl ON cl.claim_id = p.claim_id
ORDER BY p.claim_id