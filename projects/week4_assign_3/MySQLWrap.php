<?php
class MySQLWrap
{
	function __construct()
	{
		
	}

	public function getInv($filmID)
	{
		require_once("DBConnect.php");
		$conect = new DB_Connect();
		$con = $conect->connect();

		$sql = "SELECT inventory_id as invId
						FROM inventory
						where film_id = $filmID;";

		$result = $con->query($sql);
		
		$sql1 = "SELECT inventory.inventory_id
						FROM inventory join rental 
						on inventory.inventory_id = rental.inventory_id
						where film_id = $filmID and return_date is null
						group by inventory_id;";

		$result1 = $con->query($sql1);
		
		//fetch result in 2D array
		while($row = $result->fetch_row()) 
		{
		  $rows[]=$row;
		}
		
		while($row1 = $result1->fetch_row()) 
		{
		  $rows1[]=$row1;
		}

		//search arrays for value in 1st array and not in the second one
		for ($i = 0; $i < count($rows); $i++)
		{
			if (!$this->search($rows[$i][0], $rows1))
			{
				return $rows[$i][0];
			}
		}
		return -1;
	}

	public function search($number,$arr)
	{
		for ($i = 0; $i < count($arr); $i++)
		{
			if ($arr[$i][0] == $number)
			{
				return true;
			}
		}
		return false;
	}

	/*insert rent into database
		including rental table and payment table
	*/
	public function insert($tName, $filmID, $cID, $inventory)
	{
		require_once("DBConnect.php");
		$conect = new DB_Connect();
		$con = $conect->connect();

		$sql = "INSERT INTO $tName VALUES(null, null, $inventory, $cID, null, 1, null)";
		$result = $con->query($sql);
		$resultId = mysqli_insert_id($con);

		$sql2 = "SELECT * FROM film where film_id = $filmID";
		$result2 = $con->query($sql2);
		$result_row = $result2->fetch_object();
		$price = $result_row->rental_rate;

		$sql1 = "INSERT INTO payment VALUES(null, $cID, 1, $resultId, $price, null, null)";
		$result1 = $con->query($sql1);
		$conect->close();
	}

	//check if customer id is true and found in table customer
	public function checkCustomer($c_ID)
	{
		require_once("DBConnect.php");
		$conect = new DB_Connect();
		$con = $conect->connect();

		$sql = "SELECT * FROM customer where customer_id = $c_ID";
		$result = $con->query($sql);

		if ($result->num_rows > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}