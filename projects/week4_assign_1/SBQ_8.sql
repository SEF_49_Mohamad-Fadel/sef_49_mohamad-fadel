use sakila;
SELECT 
    st.store_id,
    YEAR(p.payment_date) AS year,
    MONTH(p.payment_date) AS month,
    SUM(p.amount) AS sum,
    AVG(p.amount) AS average
FROM
    payment AS p
        JOIN
    staff AS s ON p.staff_id = s.staff_id
        JOIN
    store AS st ON st.store_id = s.store_id
GROUP BY year , month , st.store_id