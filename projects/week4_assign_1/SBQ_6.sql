use sakila;
SELECT 
    c.name, COUNT(f.title) AS category
FROM
    film AS f
        JOIN
    film_category AS fc ON f.film_id = fc.film_id
        JOIN
    category AS c ON c.category_id = fc.category_id
GROUP BY c.category_id
HAVING if(count(category > 55 AND category < 65)=0,category > 65,category > 65)
ORDER BY category DESC

