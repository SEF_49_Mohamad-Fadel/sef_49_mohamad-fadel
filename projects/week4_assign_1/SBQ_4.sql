use sakila;
SELECT 
    c.first_name AS name, a.address, a.address2
FROM
    customer AS c
        JOIN
    address AS a ON c.address_id = a.address_id
WHERE
    a.address2 IS NOT NULL
ORDER BY a.address2;
