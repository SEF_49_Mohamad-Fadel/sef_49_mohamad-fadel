use sakila;
SELECT 
    c.first_name, COUNT(DISTINCT i.film_id) AS 'nb of movies'
FROM
    customer AS c
        JOIN
    rental AS r ON c.customer_id = r.customer_id
        JOIN
    inventory AS i ON i.inventory_id = r.inventory_id
GROUP BY c.first_name
ORDER BY COUNT(DISTINCT i.film_id) DESC
LIMIT 3