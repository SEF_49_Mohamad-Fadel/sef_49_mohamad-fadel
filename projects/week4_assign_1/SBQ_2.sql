use sakila;
SELECT 
    l.name, COUNT(f.title) AS 'nb of movies'
FROM
    film AS f
        JOIN
    language AS l ON f.language_id = l.language_id
WHERE
    f.release_year = '2006'
GROUP BY f.language_id
ORDER BY COUNT(f.title) DESC
LIMIT 3;