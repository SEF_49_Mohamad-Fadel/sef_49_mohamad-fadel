use sakila;
SELECT 
    CONCAT(a.first_name, ' ', a.last_name) AS name,
    COUNT(f.film_id)
FROM
    actor AS a
        JOIN
    film_actor AS fa ON a.actor_id = fa.actor_id
        JOIN
    film AS f ON f.film_id = fa.film_id
GROUP BY a.actor_id;