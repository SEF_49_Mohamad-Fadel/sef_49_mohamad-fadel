use sakila;
SELECT 
    ct.country,
    COUNT(c.customer_id) as 'country nb'
FROM
    customer AS c
        JOIN
    address AS a ON c.address_id = a.address_id
        JOIN
    city AS ci ON a.city_id = ci.city_id
        JOIN
    country AS ct ON ci.country_id = ct.country_id
GROUP BY ct.country
order by COUNT(c.customer_id) desc
limit 3;