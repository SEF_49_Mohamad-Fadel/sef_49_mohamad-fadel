use sakila;
SELECT 
    a.first_name, a.last_name, f.release_year, f.description
FROM
    actor AS a
        JOIN
    film_actor AS fa ON a.actor_id = fa.actor_id
        JOIN
    film AS f ON f.film_id = fa.film_id
WHERE
    f.description LIKE '%crocodile%shark%'
        OR f.description LIKE '%shark%crocodile%'
ORDER BY a.last_name;