
var stack = function (name, maxSize, disks)
{
	this.name = name;
	this.maxSize = maxSize;
	this.disks = [];
	this.push =  function (disk)
				{
					if (this.maxSize < 8)
					{
						this.disks[this.maxSize] = disk;
						this.maxSize++;
						document.write(disk.size + " is pushed into " + this.name + "</br>");
					}
				};
	this.pull = function ()
				{
					if (this.maxSize > 0)
					{
						var temp = this.disks[this.maxSize - 1];
						document.write(temp.size + " is pulled from " + this.name + "</br>");
						this.disks[this.maxSize - 1] = null;
						this.maxSize--;
						return temp;
					}
				};
	this.display = function ()
				{
					if (this.maxSize == 0)
					{
						document.write(this.name + " is empty </br>");
					}
					else
					{
						document.write(this.name + ": ");
						for (var i = 0; i < this.maxSize; i++)
						{
							document.write(this.disks[i].size);
						}
						document.write("</br>");
					}
				};			
};

var towerA = new stack("A", 0);
var towerB = new stack("B", 0);
var towerC = new stack("C", 0);

var disk = {size: 1};

var disk1 = Object.create(disk);
var disk2 = Object.create(disk);
var disk3 = Object.create(disk);
var disk4 = Object.create(disk);
var disk5 = Object.create(disk);
var disk6 = Object.create(disk);
var disk7 = Object.create(disk);
var disk8 = Object.create(disk);

disk1.size = 1;
disk2.size = 2;
disk3.size = 3;
disk4.size = 4;
disk5.size = 5;
disk6.size = 6;
disk7.size = 7;
disk8.size = 8;

towerA.push(disk8);
towerA.push(disk7);
towerA.push(disk6);
towerA.push(disk5);
towerA.push(disk4);
towerA.push(disk3);
towerA.push(disk2);
towerA.push(disk1);


var counter = 0;
function solveHanoi(height, towerA, towerB, towerC) 
{
    if (height >= 1) 
    {
        // Move a tower of height-1 to the buffer peg, using the destination peg.
        solveHanoi(height - 1, towerA, towerC, towerB);
		
        // Move the remaining disk to the destination peg.
        temp = towerA.pull();
        towerB.push(temp);

        // Move the tower of `height-1` from the `buffer peg` to the `destination peg` using the `source peg`
        solveHanoi(height - 1, towerC, towerB, towerA); 
       
        counter++;
    }
}

solveHanoi(8, towerA, towerB, towerC);
towerA.display();
towerB.display();
towerC.display();
document.write("It tooks " + counter + " moves to finish");
