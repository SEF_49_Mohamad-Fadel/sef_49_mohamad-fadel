

drop table Procedures;
create table Procedures( proc_id int, anest_name varchar(20), start_time varchar(10), end_time varchar(10) primary key (proc_id));

insert into Procedures values (1,"Kamal","08:00","13:30");
insert into Procedures values (2,"Kamal","09:00","15:30");
insert into Procedures values (3,"Kamal","10:00","11:30");
insert into Procedures values (4,"Kamal","12:30","13:30");
insert into Procedures values (5,"Kamal","13:30","14:30");
insert into Procedures values (6,"Kamal","18:30","19:00");

SELECT *
FROM Procedures a JOIN Procedures b 
	on a.start_time < b.end_time
	and a.end_time > b.start_time
	and a.anest_name = b.anest_name
	and a.proc_id != b.proc_id
	