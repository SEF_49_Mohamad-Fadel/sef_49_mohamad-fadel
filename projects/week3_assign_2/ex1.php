
delimiter //
drop table FiscalYearTable;
create table FiscalYearTable( fiscal_year int, start_date varchar(10), end_date varchar(10) );

DROP TRIGGER IF EXISTS start_date_check;
CREATE TRIGGER start_date_check BEFORE INSERT ON FiscalYearTable
    FOR EACH ROW
    BEGIN
        IF (NEW.start_date REGEXP '[0-9]{4}-[0-9]{2}-[0-9]{2}') = 0 THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'not a date';
        ELSEIF NEW.start_date < '1960-01-01' THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'date is less than 1960';
        ELSEIF NEW.start_date > '2020-01-01' THEN
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'date is bigger than 2020';
        END IF;
    END;

DROP TRIGGER IF EXISTS end_date_check;
CREATE TRIGGER end_date_check BEFORE INSERT ON FiscalYearTable
    FOR EACH ROW
    BEGIN
        IF (NEW.end_date REGEXP '[0-9]{4}-[0-9]{2}-[0-9]{2}') = 0 THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'not a date';
        ELSEIF NEW.end_date < '1960-01-01' THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'date is less than 1960';
        ELSEIF NEW.end_date > '2020-01-01' THEN
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'date is bigger than 2020';
        END IF;
    END;

DROP TRIGGER IF EXISTS end_date_check;
CREATE TRIGGER fiscal_year_check BEFORE INSERT ON FiscalYearTable
    FOR EACH ROW
    BEGIN
        IF (NEW.fiscal_year < NEW.start_date THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'not a date';
        END IF;
    END;

insert into FiscalYearTable values (2000,"1999-10-01","2000-09-30");
insert into FiscalYearTable values (2001,"2000-10-01","2001-09-30");
insert into FiscalYearTable values (2002,"2001-10-01","2002-09-30");
insert into FiscalYearTable values (2003,"2002-10-01","2003-09-30");
insert into FiscalYearTable values (2004,"2003-10-01","2004-09-30");
insert into FiscalYearTable values (2005,"2004-10-01","2005-09-30");
insert into FiscalYearTable values (2006,"2005-10-01","2006-09-30");
insert into FiscalYearTable values (2007,"2006-10-01","2007-09-30");
insert into FiscalYearTable values (2008,"2007-10-01","2008-09-30");
insert into FiscalYearTable values (2009,"2008-10-01","2009-09-30");
insert into FiscalYearTable values (2010,"2009-10-01","2010-09-30");
//
delimiter ;
