<?php

$a = $argv[2]; // get argument from user
echo "files within ". $a.":\n";
$dir = new DirectoryIterator(dirname($a)); // view the contents of filesystem directories
//loop over all files in directory
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) { // Determines if the current DirectoryIterator item is a directory and either . or ...  
        echo "$fileinfo"."\n";
    }
}
?>