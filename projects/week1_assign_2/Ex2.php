<?php

$log_file = '/var/log/apache2/access.log';
$fh = fopen($log_file,'r') or die($php_errormsg);

while (!feof($fh)) {
    // read each line and trim off leading/trailing whitespace
    if ($s = trim(fgets($fh,1024))) {

        $line= (explode(" ",$s));//cut line into string in an array

        $date = trim($line[3],"[");//cut [ from date 
        $d = explode("/",$date);//get date as day-month-year in array

        $dd = explode(":",$d[2]);//remove : from date
        $u = monthNb($d[1]);//call function monthNb to transfer month from letters to numbers

        $youm = date("l", mktime(0, 0, 0, "$u", "$d[0]", "$dd[0]"));//finally get the day in name 

        echo ("$line[0]"." -- "."$youm".", "."$d[1]"." "."$dd[0]"." : "."$dd[1]"."-"."$dd[2]"."-"."$dd[3]"
            ." -- "."$line[5] "."$line[6] "."$line[7]"." -- "."$line[8]"."\n");//finall print
        }
}

function monthNb($s){
        if($s == "Jan")
                return 1;
        else if ($s == "Feb")
                return 2;
        else if ($s == "March")
                return 3;
        else if ($s == "April")
                return 4;
        else if ($s == "May")
                return 5;
        else if ($s == "June")
                return 6;
        else if($s == "July")
                return 7;
        else if ($s == "Aug")
                return 8;
        else if ($s == "Seb")
                return 9;
        else if ($s == "Oct")
                return 10;
        else if ($s == "Nov")
                return 11;
        else if ($s == "Dec")
                return 12;
}
fclose($fh);
?>