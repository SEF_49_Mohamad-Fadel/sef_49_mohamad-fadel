<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@publicHomePage');
Route::get('/profile', 'UserController@index');
Route::get('/friends', 'UserController@show');
Route::get('/myProfile', 'UserController@myProfileInfo');

Auth::routes();

Route::resource('posts','PostController');

Route::resource('comments','CommentController');

Route::resource('users','UserController');

