  @extends('layouts.viewPostTemplate')

@section('title', 'View Post')

@section('content')

<div class="row">
	<br><a href="{{ url('/') }}" class="btn btn-primary">Go Back</a>
	</div>
  <div align="center">
  <p><strong>NAME:  <?php //echo $userPosts->name; ?></strong></p>
  <img src="../../storage/app/<?php echo $post->image; ?>" alt="user pic" width="300" height="300">
  <p><strong>TITLE: {{ $post->title }} </strong></p>
  <p><br>LIKES: <?php echo $post->likes; ?></p>
  <p>COMMENTS<br></p>
  <?php 
  	foreach ($p as $d) {
  ?>	    
  <div class="well well-lg" style="width:400px">
    <p align="left"><img src="../../storage/app/<?php echo $d->image; ?>" alt="user pic" width="50" height="50"><strong>&nbsp&nbsp&nbsp<?php echo ($d->name); ?></strong></p>
    <p>  <?php echo ($d->text); ?></p>
    <br>
  </div>

	<?php 
		}
	?>
	<form action="{{ route('comments.update', $post->id) }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <br><br><textarea name="commentText" style="height:60px; width:250px" required="required"></textarea><br>
    <br><button type="submit" name="comment" class="btn btn-info">ADD NEW COMMENT</button>
    <br><br>
  </form>
</div>
@endsection

