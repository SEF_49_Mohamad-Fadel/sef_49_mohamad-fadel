<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <title>@yield('title')</title>
</head>

<body>

<div class="container">
  <div class="loginBox nav navbar-nav pull-right">
    @if (Auth::guest())
    <li><a href="login">Login</a></li>
    <li><a href="register">Register</a></li>
    @else
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
      {{ Auth::user()->name }} <span class="caret"></span>
      </a>

      <ul class="dropdown-menu" role="menu">
        <li>
        <a href="../myProfile">My Profile</a>
        <a href="../profile">Profile</a>
        <a href="logout" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">Logout</a>

        <form id="logout-form" action="logout" method="POST" style="display: none;">
        {{ csrf_field() }}
        </form>
        </li>
      </ul>
    </li>
    @endif
  </div>  

  <div>
    <h1><a href="{{ url('/') }}">MandM InstaMeter</a></h1><br>
  </div>
    
  <div>
    @yield('content')
  </div>

  <div class="footer text-center" style="margin: 20px 0 60px 0;">
    <p>&copy; MandM InstaMeter 2017</p>
  </div>
</div>

</body>
</html>
