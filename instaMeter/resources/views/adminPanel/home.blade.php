@extends('layouts.template')

@section('title','Admin Panel')

@section('content')

<div class="loginBox nav navbar-nav pull-right">
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> {{ Auth::user()->name }} <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="myProfile">My Profile</a>
        <a href="profile">Profile</a>
        <a href="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
        
        <form id="logout-form" action="logout" method="POST" style="display: none;"> {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </li>
</div>

<h1>Admin Panel</h1>
<br><a href="{{ url('/') }}" class="btn btn-primary pull-left">Go Back</a>

<a href="posts/create" class="btn btn-primary pull-right">Add New Post</a>
<br><br><br>
<table class="table">
  <thead>
    <th>ID</th>
    <th>Image</th>
    <th>Title</th>
    <th>Delete</th>
  </thead>

  <tbody>
    @foreach($posts as $post)
    <tr>
      <th>{{ $post-> id }}</th>
      <td><img src="../storage/app/{{ $post->image }}" alt="user pic" width="100" height="100" /></td>
      <td>{{ $post-> title }}</td>
      <td>
      <form action="{{ route('posts.destroy', ['id'=>$post->id]) }}" method="post">
      {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">

        <input class="btn btn-danger" type="submit" value="Delete">
      </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

@endsection