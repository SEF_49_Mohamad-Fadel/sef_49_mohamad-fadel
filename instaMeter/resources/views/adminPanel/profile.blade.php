@extends('layouts.template')

@section('title','Admin Panel')

@section('content')

<div class="loginBox nav navbar-nav pull-right">
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> {{ Auth::user()->name }} <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="myProfile">My Profile</a>
        <a href="profile">Profile</a>
        <a href="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

        <form id="logout-form" action="logout" method="POST" style="display: none;"> {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </li>
</div>

<h1>Admin Profile</h1>
<br><a href="posts" class="btn btn-primary pull-left">Go Back</a>

<a href="posts/create" class="btn btn-primary pull-right">Add New Post</a>
<br><br><br>



<div class="col-sm-8 col-sm-offset-2">
  <img src="../storage/app/{{ $id }}" alt="user pic" width="250" height="250" />

  <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
    <div class="form-group"><br><br>
      <label for="title">Change Image:</label><br>
      <br><input type="file" name="image" required="required">
    </div>

    <br><br><button type="submit" class="btn btn-primary">Save</button>
  </form>
</div>

@endsection