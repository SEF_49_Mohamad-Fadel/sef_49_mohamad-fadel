@extends('layouts.publicHomePageTemplate')

@section('title', 'MandM InstaMeter')

@section('content')

<div align="center">
  <h2>Most Recent Posts</h2>
  @foreach($posts as $post)
    <a href="{{ route('posts.show', ['id'=>$post->id]) }}">
    <img src="../storage/app/{{ $post->image }}" alt="user pic" width="300" height="300"></a>
    <p>LIKES: {{ $post->likes }} &nbsp&nbsp&nbsp COMMENTS: {{ $post->comments }}</p>

    <form action="{{ route('posts.update', ['id'=>$post->id]) }}" method="post">
    {{ csrf_field() }}
      <input type="hidden" name="_method" value="PUT">
      <button type="submit" name="like" class="btn btn-info">LIKE</button>
    </form>

    <form action="{{ route('comments.update', ['id'=>$post->id]) }}" method="post">
    {{ csrf_field() }}
      <input type="hidden" name="_method" value="PUT">

      <br><br><textarea name="commentText" style="height:60px; width:250px" required="required"></textarea>
      <br><button type="submit" name="comment" class="btn btn-info">COMMENT</button>
      <br><br>
    </form>
      @endforeach

</div>

@endsection