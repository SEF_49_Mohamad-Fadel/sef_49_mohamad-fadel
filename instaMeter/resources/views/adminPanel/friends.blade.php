  @extends('layouts.viewPostTemplate')

@section('title', 'View Post')

@section('content')

<div class="row">
	<br><a href="{{ url('/') }}" class="btn btn-primary">Go Back</a>
</div>

<div align="center">
<?php 
	for($i=0;$i<count($data);$i++) {
?>	    
  <div class="well well-lg" style="width:400px">
    <p align="left"><img src="../../storage/app/<?php echo $data[$i]->image; ?>" alt="user pic" width="50" height="50"><strong>&nbsp&nbsp&nbsp&nbsp<?php echo ($data[$i]->name); ?></strong></p>

    <form action="{{ route('users.update', $data[$i]->id) }}" method="post">
      {{ csrf_field() }}
      <input type="hidden" name="_method" value="PUT">
    	<button type="submit" name="follow" class="btn btn-info pull-right" style="background-color:green" <?php echo $p ;?>>FOLLOW</button>
    </form>
    <br>
  </div>
	<?php 
		}
	?>
	
</div>
@endsection

