@extends('layouts.template')

@section('title','Add New Post')

@section('content')
<style type="text/css">
.uploader {
  position:relative; 
  overflow:hidden; 
  width:300px; 
  height:350px;
  background:#f3f3f3; 
  border:2px dashed #e8e8e8;
}

#filePhoto{
  position:absolute;
  width:300px;
  height:400px;
  top:-50px;
  left:0;
  z-index:2;
  opacity:0;
  cursor:pointer;
}

.uploader img{
  position:absolute;
  width:302px;
  height:352px;
  top:-1px;
  left:-1px;
  z-index:1;
  border:none;
}
</style>

<h1>Add New Post</h1>

<div class="col-sm-8 col-sm-offset-2">
  <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
    <div class="form-group"><br><br>

      <div class="uploader" onclick="$('#filePhoto').click()">
        <br><br>click here or drag here your images for preview 
        <img src=""/>
        <input type="file" name="image"  id="filePhoto" required="required" />
      </div>

      <br><br><label>Add Title</label>
      <br><textarea name="title" style="height:60px; width:250px" required="required"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">POST</button>
    <a href="../posts" class="btn btn-default pull-right">Go Back</a>
  </form>
</div>


<script type="text/javascript">
var imageLoader = document.getElementById('filePhoto');
  var imageName = document.getElementById('filePhoto');
  
imageLoader.addEventListener('change', handleImage, false);

function handleImage(e) {
  img = imageName.value;
  ext = img.substr((img.lastIndexOf('.') + 1));

  if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'PNG' || ext == 'JPG' || ext == 'JPEG'){
    var reader = new FileReader();
    reader.onload = function (event) {
        $('.uploader img').attr('src',event.target.result);
    }
    reader.readAsDataURL(e.target.files[0]);        
  }

  else {
    imageName.value = null;
    alert("Extension not available");
  }
}
</script>
@endsection


