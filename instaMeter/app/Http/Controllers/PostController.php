<?php

namespace myblog\Http\Controllers;

use myblog\Post;
use myblog\Like;
use myblog\Comment;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;

class PostController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function publicHomePage()
  {
    $posts = Post::all();
    $loggedUserId = Auth::id();
    $isLiked = DB::table('likes')
            ->select('post_id')
            ->where('user_id', '=', $loggedUserId)
            ->get();
        
    $posts = DB::table('posts')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->select('posts.id','posts.user_id','posts.image','posts.title','posts.likes','posts.comments','users.name')
            ->get();

    if (count($isLiked) != 0) {      
      $data = array(
        'posts' => $posts,
        'isLiked' => $isLiked
      );
      return view('blog/home', $data); 
    }
    else {
      $data = array(
        'posts' => $posts,
        'isLiked' => $isLiked
      );
      return view('blog/home', $data);
    }
  }
  
  public function index()
  {
    $posts = Post::all();
    $loggedUserId = Auth::id();
    $isLiked = DB::table('likes')
            ->select('post_id')
            ->where('user_id', '=', $loggedUserId)
            ->get();

    if (isset($_GET['up']))
    {
      $data = array(
        'posts' => $posts,
        'isLiked' => $isLiked
      );

      return view('blog/home', $data);
    }
    else {
      $posts = Post::all()->where('user_id', $loggedUserId);
      return view('adminPanel/home', ['posts'=>$posts]);
    }
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('adminPanel/create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $post = new Post;
    $user = Auth::user();

    $title = $request->title;
    $postImage = $request->file('image');
    $upload = 'images/'.$user->name.'-'.$user->id.'/';
    $postImageName = $upload.$postImage->getClientOriginalName();

    Storage::put($postImageName, file_get_contents($postImage->getRealPath()));

    $post->user_id = $user->id;
    $post->image = $postImageName;
    $post->title = $title;
    $post->likes = 0;
    $post->comments = 0;

    $post->save();

    return redirect()->route('posts.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $post = Post::find($id);

    $userPosts = DB::table('users')
            ->select('users.name')
            ->where('users.id', '=', $post->user_id)
            ->get();

    $p = DB::table('posts')
            ->join('comments', 'posts.id', '=', 'comments.post_id')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->select('comments.text','users.name','users.image')
            ->where('posts.id', '=', $id)
            ->get();

    $data = array(
        'post' => $post,
        'userPosts' => $userPosts,
        'p' => $p
    );

    return view('blog.view_post', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $post = Post::find($id);

    return view('adminPanel.edit', ['post'=>$post]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $userId = Auth::id();
    $post = Post::find($id);

    $isLiked = DB::table('likes')
            ->select('likes.post_id','likes.id')
            ->where('user_id', '=', $userId)
            ->where('post_id', '=', $id)
            ->get();

    if (isset($_POST['like'])) {
      if (count($isLiked) == 0) {
        $like = new Like;
        $like->user_id = $userId;
        $like->post_id = $id;
        $post->likes++;
        $like->save();
      }
    }

    if (isset($_POST['unlike'])) {
      $dele = $isLiked[0]->id;
        $like = Like::find($dele);
        $post->likes--;
        $like->delete();
    }

    if (isset($_POST['comment'])) {
      $post->comments++;
    }
    $post->save();

    return redirect()->route('posts.index', ['up'=>'up']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $post = Post::find($id);
    $path = "../storage/app/".$post->image;
    unlink($path);
    $post->delete();
    return redirect()->route('posts.index');
  }
}
