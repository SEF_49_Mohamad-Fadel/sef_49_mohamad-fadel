<?php

namespace myblog\Http\Controllers;

use myblog\Comment;
use myblog\Post;
use Illuminate\Http\Request;
use Auth;

class CommentController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
     //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \myblog\Comment  $comment
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, $id)
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  \myblog\Comment  $comment
   * @return \Illuminate\Http\Response
   */
  public function show(Comment $comment)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \myblog\Comment  $comment
   * @return \Illuminate\Http\Response
   */
  public function edit(Comment $comment)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \myblog\Comment  $comment
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $comment = new Comment;
    $user = Auth::user();
    $commentText = $request->commentText;

    $comment->user_id = $user->id;
    $comment->post_id = $id;
    $comment->text = $commentText;

    $comment->save();

    $post = Post::find($id);
    $post->comments++;
    $post->save();

    return redirect()->route('posts.index', ['up'=>'up']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \myblog\Comment  $comment
   * @return \Illuminate\Http\Response
   */
  public function destroy(Comment $comment)
  {
      //
  }
}
