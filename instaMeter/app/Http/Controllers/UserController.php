<?php

namespace myblog\Http\Controllers;

use Illuminate\Http\Request;
use myblog\User;
use myblog\Post;
use Auth;
use DB;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $userId = Auth::user();
    $user = User::find($userId->id);
    $image = $user->image;
    return view('adminPanel/profile', ['id'=>$image]);
  }

  public function myProfileInfo()
  {
    $userId = Auth::id();
    $posts = Post::all()->where('user_id', $userId);

    $isLiked = DB::table('likes')
            ->select('post_id')
            ->where('user_id', '=', $userId)
            ->get();

    if (count($isLiked) != 0) {      
      $data = array(
        'posts' => $posts,
        'isLiked' => $isLiked
      );
      return view('adminPanel/myProfileInfo', $data); 
    }
    else {
      $data = array(
        'posts' => $posts,
        'isLiked' => $isLiked
      );
      return view('adminPanel/myProfileInfo', $data);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $userId = Auth::user();
    $user = User::find($userId->id);
    

    $postImage = $request->file('image');
    $upload = 'images/'.$userId->name.'-'.$userId->id.'/';
    $postImageName = $upload.$postImage->getClientOriginalName();

    Storage::put($postImageName, file_get_contents($postImage->getRealPath()));

    $user->image = $postImageName;

    $user->save();

    return redirect()->route('users.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $userId = Auth::id();
    $user = User::find($userId);

    $users = DB::table('users')
        ->select('*')
        ->where('id', '!=', $userId)
        ->get();

    $followers = $user->followers;    
    $follow = explode('-', $followers);

    $data = array();
    $d = array();
    
    foreach ($users as $key) {
      for ($i=0; $i<count($follow); $i++)
      {
        if ($key->id == $follow[$i]){
          array_push($data, $key);
        }
      }
    }
    
    if ($id == 0) {
      $d = array(
            'data' => $data,
            'p' => 'disabled'
          );
      return view('adminPanel/friends', $d);
    }
    else {
      $d = array(
            'data' => $users,
            'p' => ''
          );

      return view('adminPanel/friends', $d);
    }
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
{
    $userId = Auth::id();
    $user = User::find($id);  
    $userLogedIn = User::find($userId);
    $userLogedIn->followers = $userLogedIn->followers.$user->id.'-';
    $userLogedIn->save();

    $users = DB::table('users')
        ->select('*')
        ->where('id', '!=', $userId)
        ->get();

    $d = array(
            'data' => $users,
            'p' => ''
          );

    return view('adminPanel/friends', $d);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
}
