<?php

namespace myblog\Http\Controllers;

use myblog\Post;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */


  public function publicHomePage()
  {
    $posts = Post::paginate(5);
    return view('blog/home', ['posts'=>$posts]);
  }
  
  public function index()
  {
    $loggedUserId = Auth::id();
    $posts = Post::all()->where('user_id', $loggedUserId);
    return view('adminPanel/home', ['posts'=>$posts]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('adminPanel/create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $post = new Post;
    $postTitle = $request->title;
    $postBody = $request->body;
    $postUserId = Auth::id();

    $post->user_id = $postUserId;
    $post->title = $postTitle;
    $post->body = $postBody;

    $post->save();

    return redirect()->route('posts.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $post = Post::find($id);
    $data = array(
        'id' => $id,
        'post' => $post
    );

    return view('blog.view_post', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $post = Post::find($id);

    return view('adminPanel.edit', ['post'=>$post]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $post = Post::find($id);
    $post->title = $request->title;
    $post->body = $request->body;

    $post->save();

    return redirect()->route('posts.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $post = Post::find($id);
    $post->delete();
    return redirect()->route('posts.index');
  }
}
