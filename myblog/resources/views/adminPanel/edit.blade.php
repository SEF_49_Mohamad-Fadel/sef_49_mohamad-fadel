@extends('layouts.template')

@section('title','Edit Post #' . $post->id)

@section('content')
<h1>Edit Post # {{ $post->id }}</h1>

<div class="col-sm-8 col-sm-offset-2">
  <form action="{{ route('posts.update', ['id'=>$post->id]) }}" method="post">
  {{ csrf_field() }}

    <input type="hidden" name="_method" value="PUT">
    <div class="form-group">
      <label for="title">Title:</label>
      <input type="text" name="title" class="form-control" value="{{ $post->title }}" required="required">
    </div>

    <div class="form-group">
      <label for="body">Body:</label>
      <textarea name="body" class="form-control" cols="30" rows="10" required="required">{{ $post->body }}</textarea>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="../" class="btn btn-default pull-right">Go Back</a>
  </form>
</div>

@endsection