@extends('layouts.template')

@section('title','Add New Post')

@section('content')
<h1>Add New Post</h1>

<div class="col-sm-8 col-sm-offset-2">
  <form action="{{ route('posts.store') }}" method="post">
  {{ csrf_field() }}
    <div class="form-group">
      <label for="title">Title:</label>
      <input type="text" name="title" class="form-control" required="required">
    </div>

    <div class="form-group">
      <label for="body">Body:</label>
      <textarea name="body" class="form-control" cols="30" rows="10" required="required"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="../posts" class="btn btn-default pull-right">Go Back</a>
  </form>
</div>

@endsection