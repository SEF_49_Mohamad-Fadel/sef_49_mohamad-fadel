@extends('layouts.viewPostTemplate')

@section('title', 'View Post #'. $id)

@section('content')
	<br><br>
	<h1>{{ $post->title }}</h1>
	<br><br>
	<p>{{ $post->body}}</p>
	<br><br>

	<div class="row" align="center">
		<br><a href="{{ url('/') }}" class="btn btn-primary">Go Back</a>
	</div>

@endsection