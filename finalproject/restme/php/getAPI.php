<?php

if (isset($_POST['url']) && isset($_POST['method']) && isset($_POST['bodyText'])) 
{
  $url = $_POST['url'];
  $method = $_POST['method'];
  $bodyText = $_POST['bodyText'];
  getRequest($url, $method, $bodyText);
}

else if (isset($_POST['id']))
{
  $id = $_POST['id'];
  require_once("DBConnect.php");
  $conect = new DB_Connect();
  $con = $conect->connect();

  $sql = "SELECT * FROM requests WHERE id = '$id'";
  $result = $con->query($sql);
  $result_row = $result->fetch_object();

  $url = $result_row->request_url;
  $method = $result_row->request_method;
  $bodyText = $result_row->request_body;
  getRequest($url, $method, $bodyText);
}

function getRequest($url, $method, $bodyText, $test) {

  if ($method == "GET" || $method == "get") {
      $options = array(
      CURLOPT_HEADER => true,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
    );

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    $content = curl_exec($ch);
    curl_close($ch);

  }

  else if ($method == "POST" || $method == "post") {
    $options = array(
      CURLOPT_HEADER => true,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => $bodyText,
    ); 

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    $content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    curl_close($ch);

    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['content'] = $content;

  }
  
  else if ($method == "PUT" || $method == "put") {
    $options = array(
      CURLOPT_HEADER => true,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
      //CURLOPT_POST => true,
      //CURLOPT_POSTFIELDS => $bodyText,
    ); 

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    //curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($bodyText));
    curl_setopt($ch, CURLOPT_POSTFIELDS,$bodyText);
    $content = curl_exec($ch);

    curl_close($ch);
  }

  else if ($method == "DELETE" || $method == "delete") {
    $options = array(
      CURLOPT_HEADER => true,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
      //CURLOPT_POST => true,
      //CURLOPT_POSTFIELDS => $bodyText,
    ); 

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_POSTFIELDS,$bodyText);
    $content = curl_exec($ch);

    curl_close($ch);
  }

  else {
    $content = "error";
  }

  if (isset($test) && $test == "test") {
    $text = explode("\n", $content);

    if (strcmp($text[0], 'HTTP/1.1 200 OK') == 1) {
      echo "OK";
    }
    else {
      echo "error";
    } 
  }
  else {
    echo $content;
  }
  
}
