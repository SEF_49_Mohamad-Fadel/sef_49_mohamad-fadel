<?php

if (isset($_POST['url']) && isset($_POST['method']) && isset($_POST['bodyText']) && isset($_POST['testFunction'])) 
{
  $url = $_POST['url'];
  $method = $_POST['method'];
  $bodyText = $_POST['bodyText'];
  $testFunction = $_POST['testFunction'];;
  getRequest($url, $method, $bodyText, $testFunction);
}

else if (isset($_POST['id']))
{
  $id = $_POST['id'];
  require_once("DBConnect.php");
  $conect = new DB_Connect();
  $con = $conect->connect();

  $sql = "SELECT * FROM favourites WHERE id = '$id'";
  $result = $con->query($sql);
  $result_row = $result->fetch_object();

  $url = $result_row->request_url;
  $method = $result_row->request_method;
  $bodyText = $result_row->request_body;
  getRequest($url, $method, $bodyText);
}

function getRequest($url, $method, $bodyText, $testFunction, $test) {

  $testoutput = testing($url, $testFunction);

  if ($method == "GET" || $method == "get") {
      $options = array(
        CURLOPT_HEADER => true,    // don't return headers
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_AUTOREFERER => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT => 120,      // timeout on response
        CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
      );

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    $content = curl_exec($ch);

    $pl = $testoutput."-----".$url."-----".$method."-----".$bodyText."-----".$content;

    //$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    //$header = substr($content, 0, $header_size);
    //$body = substr($content, $header_size);

    //list($headers, $co) = explode("\r\n\r\n", $content, 2);

    curl_close($ch);

  }

  else if ($method == "POST" || $method == "post") {
    $options = array(
      CURLOPT_HEADER => true,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => $bodyText,
    ); 

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    $content = curl_exec($ch);

    $pl = $url."-----".$method."-----".$bodyText."-----".$content;

    curl_close($ch);


  }
  
  else if ($method == "PUT" || $method == "put") {
    $options = array(
      CURLOPT_HEADER => true,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
      //CURLOPT_POST => true,
      //CURLOPT_POSTFIELDS => $bodyText,
    ); 

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    //curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($bodyText));
    curl_setopt($ch, CURLOPT_POSTFIELDS,$bodyText);
    $content = curl_exec($ch);

    $pl = $url."-----".$method."-----".$bodyText."-----".$content;

    curl_close($ch);
  }

  else if ($method == "DELETE" || $method == "delete") {
    $options = array(
      CURLOPT_HEADER => true,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
      //CURLOPT_POST => true,
      //CURLOPT_POSTFIELDS => $bodyText,
    ); 

    $ch = curl_init($url);
    curl_setopt_array($ch,$options);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_POSTFIELDS,$bodyText);
    $content = curl_exec($ch);

    $pl = $url."-----".$method."-----".$bodyText."-----".$content;

    curl_close($ch);
  }

  else {
    $content = "error";
  }

  if (isset($test) && $test == "test") {
    $text = explode("\n", $content);

    if (strcmp($text[0], 'HTTP/1.1 200 OK') == 1) {
      echo $text[0];
    }
    else {
      echo "error";
    } 
  }
  else {
    echo($pl);
  }
  
}

function testing($url, $testFunction) {
  $options = array(
      CURLOPT_HEADER => false,    // don't return headers
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_AUTOREFERER => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT => 120,      // timeout on response
      CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
    );

  $ch = curl_init($url);
  curl_setopt_array($ch,$options);
  $testoutput = curl_exec($ch);

  $outarray = explode("}", $testoutput);

  //if($size==3) echo'true';else echo'false';
  $size = count($outarray) - 1;

  //if($isJson) echo'true';else echo'false';
  $isJson = is_array(json_decode($testoutput, true));

  ob_start();
  eval($testFunction);
  $t = ob_get_contents();

  //if($isJson) echo'true,json object'. PHP_EOL;else echo'false,not json object'. PHP_EOL;if($size==3) echo'correct output size';else echo'false output size';
}