
function urlInput() {
  var url = document.getElementById('URL').value;
  var urllength = url.replace(/\s+/g, '');

  if(urllength.length == 0) {
    errorDialog("Sorry Invalid URL !!");
  }
  else {
    removeDialog();
    return url;
  }
}

function methodInput() {
  var methodList = document.getElementById("method");
  var method = methodList.options[methodList.selectedIndex].text;
  return method;
}

function bodyInput() {
  var bodyText = document.getElementById('bodyText').value;
  return bodyText;
}

function errorDialog(text) {
  document.getElementById('errordialog').style.display = "block";
  document.getElementById('errortext').innerHTML = text;
}

function removeDialog() {
  document.getElementById('errordialog').style.display = "none";
  document.getElementById('errortext').innerHTML = ""; 

}