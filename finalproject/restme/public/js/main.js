// Script to open and close sidebar
function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("myOverlay").style.display = "none";
}

function showit(id) {
  if (id == 1) {
    document.getElementById('headcontent').style.display = 'block';
    document.getElementById('bodycontent').style.display = 'none';
    document.getElementById('testtcontent').style.display = 'none';

    document.getElementById('resheader').style.background= 'yellow';
    document.getElementById('resbody').style.background= 'lightgray';
    document.getElementById('tesbody').style.background= 'lightgray';
  }

  else if (id == 2) {
    document.getElementById('headcontent').style.display = 'none';
    document.getElementById('bodycontent').style.display = 'block';
    document.getElementById('testtcontent').style.display = 'none';

    document.getElementById('resheader').style.background= 'lightgray';
    document.getElementById('resbody').style.background= 'yellow';
    document.getElementById('tesbody').style.background= 'lightgray';
  }

  else if (id == 3) {
    document.getElementById('headcontent').style.display = 'none';
    document.getElementById('bodycontent').style.display = 'none';
    document.getElementById('testtcontent').style.display = 'block';
    
    document.getElementById('resheader').style.background= 'lightgray';
    document.getElementById('resbody').style.background= 'lightgray';
    document.getElementById('tesbody').style.background= 'yellow';
  }
}

function showitB(id) {
  if (id == 1) {
    document.getElementById('bodyTcontent').style.display = 'block';
    document.getElementById('testcontent').style.display = 'none';
    document.getElementById('headercontent').style.display = 'none';

    document.getElementById('bheader').style.background= 'yellow';
    document.getElementById('theader').style.background= 'lightgray';
    document.getElementById('hheader').style.background= 'lightgray';
  }

  else if (id == 2) {
    document.getElementById('bodyTcontent').style.display = 'none';
    document.getElementById('testcontent').style.display = 'block';
    document.getElementById('headercontent').style.display = 'none';

    document.getElementById('bheader').style.background= 'lightgray';
    document.getElementById('theader').style.background= 'yellow';
    document.getElementById('hheader').style.background= 'lightgray';
  }

  else if (id == 3) {
    document.getElementById('bodyTcontent').style.display = 'none';
    document.getElementById('testcontent').style.display = 'none';
    document.getElementById('headercontent').style.display = 'block';

    document.getElementById('bheader').style.background= 'lightgray';
    document.getElementById('theader').style.background= 'lightgray';
    document.getElementById('hheader').style.background= 'yellow';
  }
}

//document.getElementById('submit').addEventListener('click', submit);