
function testAll(id) {
  var request = new XMLHttpRequest();
    request.open("POST", "php/testAll.php", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function() {
      if (request.readyState === 4) {
        if (request.status === 200) {
          document.body.className = 'ok';
          var content = request.responseText;
          if (content == "error")
          {
            document.getElementById('rhead').innerHTML = "error";
            document.getElementById('rbody').innerHTML = "error";
          }
          else {
            document.getElementById('rhead').innerHTML = "OK";
            document.getElementById('rbody').innerHTML = "OK";
          }
        }
        else {
          var content = request.responseText;
          document.getElementById('rhead').innerHTML = content;
          document.getElementById('rbody').innerHTML = "error";
        }
      }
      else {
        var content = request.responseText;
        document.getElementById('rhead').innerHTML = content;
        document.getElementById('rbody').innerHTML = "error";
      }
    }
    request.send('idTest=' + id);
}

function submitFav(id) { 
  var request = new XMLHttpRequest();
    request.open("POST", "php/getAPI.php", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function() {
      if (request.readyState === 4) {
        if (request.status === 200) {
          document.body.className = 'ok';
          var content = request.responseText;
          if (content == "error")
          {
            document.getElementById('rhead').innerHTML = "error";
            document.getElementById('rbody').innerHTML = "error";
          }
          else if (content) {
            parseC(content);
            var header = saveHeader();
          }
          else {
            document.getElementById('rhead').innerHTML = "error";
            document.getElementById('rbody').innerHTML = "error";
          }
        }
        else {
          var content = request.responseText;
          document.getElementById('rhead').innerHTML = content;
          document.getElementById('rbody').innerHTML = "error";
        }
      }
      else {
        var content = request.responseText;
        document.getElementById('rhead').innerHTML = content;
        document.getElementById('rbody').innerHTML = "error";
      }
    }
    request.send('id=' + id);
}

function submit() {
  var url = urlInput();
  var method = methodInput();
  var bodyText = bodyInput();
  var header = saveHeader();
  var testFunction = document.getElementById('bodyTest').value;
  //alert(eval(testFunction));

  if (url) {
    var request = new XMLHttpRequest();
    request.open("POST", "php/getAPI.php", true);
    request.setRequestHeader(header[0], header[1]);
    //request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function() {
      if (request.readyState === 4) {
        if (request.status === 200) {
          document.body.className = 'ok';
          var content = request.responseText;
          if (content == "error")
          {
            document.getElementById('rhead').innerHTML = method + bodyText;
          }
          else {
            parse(content);
          }
        }
        else {
          var content = request.responseText;
          document.getElementById('rhead').innerHTML = content;
          document.getElementById('rbody').innerHTML = "error";
        }
      }
      else {
        var content = request.responseText;
        document.getElementById('rhead').innerHTML = content;
        document.getElementById('rbody').innerHTML = "error";
      }
    }
    request.send('url=' + url + '&method=' + method + '&bodyText=' + bodyText + '&testFunction=' + testFunction);
  }
}


function saveHeader() {
  var headerName = document.getElementById('headername').value;
  var headerValue = document.getElementById('headervalue').value;

  var header = [headerName, headerValue];
  document.getElementById('myModal').style.display = "none";
  displayHeader(header);
  return header;
}

function displayHeader(header) {
  var headerdisplay = parseHeader(header);
  document.getElementById('headertext').innerHTML = headerdisplay;
}

function parseHeader(header) {
  var headerName = header[0];
  var headerValue = header[1];
  return headerName + ": " + headerValue;
}

function deleteheader() {
  document.getElementById('headertext').innerHTML = "";
  document.getElementById('headers').style.display = "none";
  document.getElementById('headername').value = "Content-Type";
  document.getElementById('headervalue').value = "application/x-www-form-urlencoded";
}

function parse(content) {
  //var text = content.split("\n");
  display(content);
}

function parseC(content) {
  var past = content.split("-----");
  var text = past[4].split("\n");
  var final = "status code: " + text[0] + "\n";
  final += text[1] + "\n";
  final += text[2] + "\n";
  final += text[3] + "\n";
  final += text[4] + "\n";
  final += text[5] + "\n";
  final += text[6] + "\n";

  document.getElementById('URL').value = past[1];
  document.getElementById('method').value = past[2];
  document.getElementById('bodyText').innerHTML = past[3];

  document.getElementById('rbody').innerHTML = " ";
  document.getElementById('rhead').innerHTML = final;
  for(var i=7;i<text.length;i++)
    document.getElementById('rbody').innerHTML += text[i] + "\n";
}

function display(content) {
  var past = content.split("-----");
  var text = past[4].split("\n");
  var final = "status code: " + text[0] + "\n";
  final += text[1] + "\n";
  final += text[2] + "\n";
  final += text[3] + "\n";
  final += text[4] + "\n";
  final += text[5] + "\n";
  final += text[6] + "\n";

  document.getElementById('rbody').innerHTML = " ";
  document.getElementById('rhead').innerHTML = final;
  document.getElementById('ttbody').innerHTML = past[0];

  for(var i=7;i<text.length;i++)
    document.getElementById('rbody').innerHTML += text[i] + "\n";
}
