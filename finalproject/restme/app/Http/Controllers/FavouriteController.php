<?php

namespace App\Http\Controllers;

use App\favourite;
use App\package;
use Auth;
use DB;
use Illuminate\Http\Request;

class FavouriteController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $id = Auth::id();
    $fav = new favourite;

    if ($request->packagename) {
      $packageId = $this->checkPackage($request->packagename);
    }

    else {
      $packageId = $this->checkPackage("history");
    }
    $fav->user_id = $id;
    $fav->package_id = $packageId;
    $fav->request_url = $request->URL;
    $fav->request_method = $request->method;
    $fav->request_body = $request->body;
    
    if ($request->requestname) {
      $fav->request_name = $request->requestname;
    }
    else {
      $reqname = $request->URL;
      $reqname = explode("/", $reqname);
      $fav->request_name = $reqname[0];
    }

    $fav->save();

    return view("home");
  }

  function checkPackage($package_name) {
    $package = new package;
    $id = Auth::id();


    $packageId = 0;
    $pacid = DB::table('packages')
            ->select('package_id')
            ->where('package_name', '=', $package_name)
            ->where('user_id', '=', $id)
            ->get();

    
    if (count($pacid) > 0) {
      return $pacid[0]->package_id;
    }

    else {
      $package->user_id = $id;
      $package->package_name = $package_name;
      $package->save();
      $packageId = $package->id;
    }
    return $packageId;
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\favourite  $favourite
   * @return \Illuminate\Http\Response
   */
  public function show(favourite $favourite)
  {
    return "ppppppppppppppppppppppp";
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\favourite  $favourite
   * @return \Illuminate\Http\Response
   */
  public function edit(favourite $favourite)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\favourite  $favourite
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, favourite $favourite)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\favourite  $favourite
   * @return \Illuminate\Http\Response
   */
  public function destroy(favourite $favourite)
  {
    //
  }
}
