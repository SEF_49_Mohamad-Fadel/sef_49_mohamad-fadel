@extends('layouts.app')

@section('content')

<div class="" style="max-width:1500px">

<body class="w3-content" style="max-width:1500px">

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-bar-block w3-white w3-collapse w3-top" style="z-index:3;width:250px;margin-top:50px;" id="mySidebar">
  <div class="w3-container w3-display-container w3-padding-16">
    <i onclick="w3_close()" class="fa fa-remove w3-hide-large w3-button w3-display-topright"></i>
    <h3 class="w3-wide"><b>FAVOURITES</b></h3>
  </div>

  <?php 
    require_once("php/DBConnect.php");

    $id = Auth::id();
    $conect = new DB_Connect();
    $con = $conect->connect();
    $sql = "SELECT package_name AS name, package_id AS id
            FROM packages
            where user_id = $id";

    $sql2 = "SELECT *
             FROM favourites
             where user_id = $id";

    $result = $con->query($sql);
    $result2 = $con->query($sql2);

    while($result_row = $result->fetch_object()) {
      $result2 = $con->query($sql2);
  ?>

  <div class="w3-large" style="font-weight:bold">
    <button onclick="myFunction('<?php echo "$result_row->name"; ?>',<?php echo "$result_row->id"; ?>)" class="w3-bar-item w3-button" style="float: left;width:160px;"><div id="<?php echo "$result_row->id"; ?>" style="float: left;margin-right: 10px;"> > </div><?php echo "$result_row->name"; ?>
      </button>

      <button onclick="testAll(<?php echo "$result_row->id"; ?>)" class="fa fa-check-square-o w3-margin-right" style="float: right;margin-top: 10px;" title="Test All"></button>

      <div id="<?php echo "$result_row->name"; ?>" class="w3-hide">
        <?php while($result_row2 = $result2->fetch_object()) {
          if ($result_row->id == $result_row2->package_id) {
        ?>
        <a onclick='submitFav(<?php echo "$result_row2->id"; ?>)' class="w3-button w3-block w3-left-align" style="margin-left: 20px;"><?php echo "$result_row2->request_name"; }?></a>
        <?php }?>
      </div>
    <?php } ?>

  </div>
</nav>

<!-- Top menu on small screens -->
<header class="w3-bar w3-top w3-hide-large w3-gray w3-large" style="margin-top: 50px;">
  <div class="w3-bar-item w3-padding-24 w3-wide">FAVOURITES</div>
  <a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding-24 w3-right" onclick="w3_open()"><i class="fa fa-bars"></i></a>
</header>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:250px">

  <!-- Push down content on small screens -->
  <div class="w3-hide-large" style="margin-top:83px"></div>
  
  <!-- Top header -->
  <header class="w3-container w3-xlarge" style="margin-top:50px;">
  <p class="w3-left">REQUEST</p>
  <p class="w3-right">
    <button  id="myBtnS" class="fa fa-star-o w3-margin-right" title="Save"></button>
  </p>
  </header>

  <!-- REQUEST -->
  <div class="w3-container" id="request" style="margin-top: 0;">
    <div style="height:180px;">
      <br/><button id="myBtn" style="margin-left: 10px">Header</button>

      <!-- The Modal -->
      <div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
          <div class="modal-header">
            <span class="close">&times;</span>
            <h2>Request Header</h2>
          </div>
          <div class="modal-body">
            <label>Name:</label><br/>
            <input id="headername" name="headername" type="text" size="40" value="Content-Type">
            <br/><br/><label>Value:</label><br/>
            <input id="headervalue" name="headervalue" type="text" size="40" value="application/x-www-form-urlencoded">
          </div>
          <br/><div class="modal-footer">
            <button id="header" type="submit" onclick="saveHeader()" class="w3-button-default w3-large w3-white">OK</button>
          </div>
        </div>
      </div>

      <form action="{{ route('favourites.store') }}" method="POST">
      {{ csrf_field() }}
        <div id="myModalS" class="modalS">
          <!-- Modal content -->
          <div class="modal-contentS">
            <div class="modal-headerS">
              <span class="closeS">&times;</span>
              <h2>Favourites</h2>
            </div>
            <div class="modal-bodyS">
              <label>Package Name:</label><br/>
              <input name="packagename" type="text" size="40">
              <br/><br/><label>Request Name:</label><br/>
              <input name="requestname" type="text" size="40">
            </div>
            <br/><div class="modal-footerS" style="height: 40px">
              <button id="saveS" type="submit" class="w3-button-default w3-large w3-white" style="float: right;">Save</button>
            </div>
          </div>
        </div>
      
        <br/>
        <div id="errordialog">
          <label id="errortext" type="text" readonly></label>
        </div>
        <br/>
          <div class="form-group" style="margin-left: 10px">
              <select id="method" name="method">
                <option value="GET">GET</option>
                <option value="POST">POST</option>
                <option value="PUT">PUT</option>
                <option value="DELETE">DELETE</option>
              </select>
            <input id="URL" type="text" name="URL" size="60" placeholder="URL" required>
      </form>

        </div>
        <button id="submit" onclick="submit()" class="w3-button-default w3-large w3-red" style="float: right; margin-right: 10px;">Send</button>
    </div>

       <!-- BODY -->
        <div class="" style="margin-top: 0">
          <input id="bheader" class="btn btn-default" type="button" value="Body" onclick="showitB(1)" style="background-color: lightgray">
          <input id="theader" class="btn btn-default" type="button" value="Tests" onclick="showitB(2)" style="background-color: lightgray">
          <input id="hheader" class="btn btn-default" type="button" value="Headers" onclick="showitB(3)" style="background-color: lightgray">

          <div id="bodyTcontent" style="margin-top: 0">
            <textarea id="bodyText" type="text"></textarea>
          </div>

          <div id="testcontent" style="display: none;">
            <textarea id="bodyTest" type="text"></textarea>
          </div>

          <div id="headercontent" style="display: none;">
            <textarea id="headertext" type="text" style="width: 1100px;height: 150px;resize: none;"></textarea>
          </div>
        </div>
        <!-- End BODY -->
  </div>
  <!-- End Request -->


  <!-- Response -->
  <div class="" id="response" style="margin-top: 10px;margin-left: 15px">
    <input id="resheader" class="btn btn-default" type="button" value="Response Header" onclick="showit(1)" style="background-color: lightgray">
    <input id="resbody" class="btn btn-default" type="button" value="Response Body" onclick="showit(2)" style="background-color: lightgray">
    <input id="tesbody" class="btn btn-default" type="button" value="Test Response" onclick="showit(3)" style="background-color: lightgray">

    <div id="headcontent" style="margin-top: 0">
      <textarea id="rhead" type="text" readonly></textarea>
    </div>

    <div id="bodycontent" style="display: none;">
      <textarea id="rbody" type="text" readonly></textarea>
    </div>

    <div id="testtcontent" style="display: none;">
      <textarea id="ttbody" type="text" readonly></textarea>
    </div>
  </div>
  <!-- End Responce -->
  
  <!-- Footer -->
  <div class="w3-white w3-center w3-padding-24" style="width:1200px;">Powered by <a href="#" title="MandM" target="_blank" class="w3-hover-opacity">MandM</a></div>
 
  <!-- End page content -->
</div>

<script>

// Script to open and close sidebar
function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("myOverlay").style.display = "none";
}
</script>

<script>
function myFunction(id, id2) {
    var x = document.getElementById(id);
    var y = document.getElementById(id2);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        y.innerHTML = "^";
    } else { 
        x.className = x.className.replace(" w3-show", "");
        y.innerHTML = ">";
    }
}
</script>

</div>
@endsection
