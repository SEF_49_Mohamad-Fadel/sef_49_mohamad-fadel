<?php

class sqlController
{
  public function select($data)
  {
  	$sql = "";
	  $data = explode("=", $data);
    $keyword = $data[0];
    $attributes = explode(',', trim($data[1],'{}'));
    $sql = $keyword . $attributes
		return $sql;
  }

  public function selectQuery($method, $table, $queryString)
  {
	$con = $this->DBConnection();

	$queryString = explode("&", $queryString);
	$flag = "";
	for ($i = 0; $i < count($queryString); $i++) {
	  $qString = explode("=", $queryString[$i]);
	  $keyword = $qString[0];
	  $attributes = $qString[1];

	  switch ($keyword) {
		case 'fields':
		  echo  "field";
		  $flag =  "fields";
		  $sql = "select $attributes from `$table`";  
		  break;

		case 'orderby':
		  echo "order";
		  if ($flag == "fields") {
			$sql .= " order by $attributes"; 
			break;
		  }
		  else {
			$sql = "select * from `$table` order by $attributes";
			break;
		  }

		case 'sort':
		  echo "sort";
		  $sql .= " $attributes"; 
		  break;

		case 'search':
		  echo "search";
		  $searchedfield = $qString[1];
		  $attributes = $qString[2];
		  $sql = "select * from `$table` where $searchedfield like '$attributes'";   
		  break;
	  }
	}
	
	$result = mysqli_query($con,$sql);
	$this->display($method, $queryString, $result);
	$con->close();
  }

  public function insert($method, $table, $input)
  {
	$con = $this->DBConnection();
	
	if ($table == "film") {
	  require_once("models/Film.php");
	  $film = new Film;
	  $filmArray = $film->filmArray;

	  $attribute = explode(",", $input);

	  for($i = 0; $i < count($attribute); $i++) {
		$field = explode("=", $attribute[$i]);
		for ($j = 0; $j < count($filmArray); $j++) {
		  if ($field[0] == $filmArray[$j]) {
			$filmArray[$j+1] = $field[1];
		  }
		}
	  }

	  $film->filmArray = $filmArray;
	  $result = $film->store();
	  $this->display($method, $film, $result);
	}
	else {
	  //URL = http://localhost/api/index.php/film
	  //BODY = "title=\"hahaha\",description=\"desc\",release_year=1,language_id=1"
	  $sql = "INSERT INTO $table set $input";

	  $result = mysqli_query($con,$sql);
	  $this->display($method, $input, $result);
	  $con->close();
	}
  }

  public function update($method, $table, $dataUpdate, $input)
  {
	$con = $this->DBConnection();

	if ($table == "film") {
	  require_once("models/Film.php");
	  $film = new Film;

	  $dataUpdate = explode("=", $dataUpdate);
	  $field = $dataUpdate[0];
	  $id = $dataUpdate[1];

	  $filmUpdated = $film->update($id, $input);
	  $this->display($method, $filmUpdated, $result);
	}
  
	else {
	  $dataUpdate = explode("=", $dataUpdate);
	  $field = $dataUpdate[0];
	  $id = $dataUpdate[1];

	  //URL = http://localhost/api/index.php/film/film_id=1009
	  //BODY = "title=\"lol\",description=\"no\",release_year=2001,language_id=4"
	  $sql = "update $table set $input where $field = $id";

	  $result = mysqli_query($con,$sql);
	  $this->display($method, $input, $result);
	  $con->close();
	}
  }

  public function delete($method, $table, $toDelete)
  {
	$con = $this->DBConnection();

	if ($table == "film") {
	  require_once("models/Film.php");
	  $film = new Film;

	  $data = explode("=", $toDelete);
	  $field = $data[0];
	  $id = $data[1];

	  $filmDeleted = $film->delete($id);
	  $this->display($method, $filmDeleted, $result);
	}
  
	else {
	  $data = explode("=", $toDelete);
	  $field = $data[0];
	  $id = $data[1];
	  
	  //URL = http://localhost/api/index.php/film/film_id=1011
	  //BODY = "film_id=1011"
	  $sql = "delete from $table where $field = $id";

	  $result = mysqli_query($con,$sql);
	  $this->display($method, $input, $result);
	  $con->close();
	}
  }
}