<?php 
require_once("libs/Controller.php");

class Error extends Controller {
	public function IndexAction() {
		$this->view->message = "Controller doesn't exist !!";
		$this->view->render('views/error/index.phtml');
	}
}