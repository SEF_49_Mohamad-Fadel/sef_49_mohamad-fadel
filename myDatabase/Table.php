<?php

class Table
{
  protected $name;

  function __construct()
  {
    $this->name = "Fadel's Table";
  }

  function createTable($databaseName, $tableName, $line)
  {
    if (!file_exists("$databaseName/$tableName.csv"))
    {
      $handle = fopen("$databaseName/$tableName.csv", "a");
      fputcsv($handle, $line);
      fclose($handle);
      echo "Table is Created WoW!\n";
    }
    else
    {
      echo "Sorry Table is Created Before :(\n";
    }
  }

  function TbExists($databaseName, $tableName)
  {
    if (file_exists("$databaseName/$tableName.csv"))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  function addRecord($databaseName, $tableName, $record)
  {
    $handle = fopen("$databaseName/$tableName.csv", "r");
    $found = false;
    $myCsv = array();
    while (!feof($handle)) 
    {
      $myCsv = fgetcsv($handle, 1024); 
      if ($myCsv != false)
      {
        if ($record[0] == $myCsv[0]) 
        {
          echo "Sorry ID is Taken :(\nCan't Insert Two Identical ID :(\n";
          $found = true;
          return;
        }
      }
    }
    if (!$found)
    {
      $handle = fopen("$databaseName/$tableName.csv", "a");
      fputcsv($handle, $record);
      fclose($handle);
      echo "Record is Added WoW!\n";
    }
  }

  function deleteTable($databaseName, $tableName)
  {
    unlink("$databaseName/$tableName.csv");
    echo "Table is Deleted :(";
  }

  function deleteRecord($databaseName, $tableName, $id)
  {
    $handle = fopen("$databaseName/$tableName.csv", "r"); 
    $handleT = fopen("$databaseName/name.csv", "w");

    $myCsv = array();
    while (!feof($handle)) 
    {
      $myCsv = fgetcsv($handle, 1024); 
      if($myCsv != false)
      {
        if ($id != $myCsv[0]) 
        {
          fputcsv($handleT, $myCsv);
        }
      }
    }
    fclose($handle);
    fclose($handleT);
    unlink("$databaseName/$tableName.csv");
    rename("$databaseName/name.csv","$databaseName/$tableName.csv");
    echo "Record deleted WOW\n";
  }

  function retrieveRecord($databaseName, $tableName, $id)
  {
    $handle = fopen("$databaseName/$tableName.csv", "r"); 
    $myCsv = array();
    while (!feof($handle)) 
    {
      $myCsv = fgetcsv($handle, 1024); 
      if ($myCsv != false)
      {
        if ($id == $myCsv[0]) 
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            echo $myCsv[$i] . " ";
          }
          echo "\n";
        }
      }
    }
    fclose($handle);
  }

  function getRecord($databaseName, $tableName, $recordName, $id)
  {
    $handle = fopen("$databaseName/$tableName.csv", "r"); 
    $myCsv = array();
    $index = -1;
    $found = false;
    $key = fgetcsv($handle, 1024);
    for ($j = 0; $j < count($key); $j++)
    {
      if ($recordName == $key[$j]) 
      {
        $index = $j;
        $found = true;
      }
    }

    if (!$found)
    {
      echo "Sorry Record Name Not Found :(\n";
      return;
    }

    while (!feof($handle)) 
    {
      $myCsv = fgetcsv($handle, 1024); 
      if ($myCsv != false)
      {
        if ($id == $myCsv[$index]) 
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            echo $myCsv[$i] . " ";
          }
          echo "\n";
        }
      }
    }
    fclose($handle);
  }

  function indexRecord($databaseName, $tableName, $recordName)
  {
    $handle = fopen("$databaseName/$tableName.csv", "r"); 
    $aToi = array();
    $aToiIndex = 0;
    $jTor = array();
    $jTorIndex = 0;
    $sToz = array();
    $sTozIndex = 0;
    $key = fgetcsv($handle);
    $index = -1;

    $aToiFile = fopen("$databaseName/aToi.csv", "a");
    fputcsv($aToiFile, $key);

    $jTorFile = fopen("$databaseName/jTor.csv", "a");
    fputcsv($jTorFile, $key);

    $sTozFile = fopen("$databaseName/sToz.csv", "a");
    fputcsv($sTozFile, $key);

    for ($j = 0; $j < count($key); $j++)
    {
      if ($recordName == $key[$j]) 
      {
        $index = $j;
      }
    }

    while (!feof($handle)) 
    {
      $myCsv = fgetcsv($handle); 
      if ($myCsv != false)
      {
        if (ord($myCsv[$index]) > 96 && ord($myCsv[$index]) < 106)
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            $aToi[$aToiIndex] = $myCsv[$i];
            $aToiIndex++;
          }
          $aToiFile = fopen("$databaseName/aToi.csv", "a");
          fputcsv($aToiFile, $aToi);
          unset($aToi);
          fclose($aToiFile);
        }
        else if (ord($myCsv[$index]) > 105 && ord($myCsv[$index]) < 115)
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            $jTor[$jTorIndex] = $myCsv[$i];
            $jTorIndex++;
          }
          $jTorFile = fopen("$databaseName/jTor.csv", "a");
          fputcsv($jTorFile, $jTor);
          unset($jTor);
          fclose($jTorFile);
        }
        else if (ord($myCsv[$index]) > 114 && ord($myCsv[$index]) < 123)
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            $sToz[$sTozIndex] = $myCsv[$i];
            $sTozIndex++;
          }
          $sTozFile = fopen("$databaseName/sToz.csv", "a");
          fputcsv($sTozFile, $sToz);
          unset($sToz);
          fclose($sTozFile);
        }
      }
    }
    fclose($handle);
  }

  function indexId($databaseName, $tableName)
  {
    $handle = fopen("$databaseName/$tableName.csv", "r"); 
    $one = array();
    $oneIndex = 0;
    $two = array();
    $twoIndex = 0;
    $three = array();
    $threeIndex = 0;
    $key = fgetcsv($handle);
    $index = -1;

    $oneFile = fopen("$databaseName/one.csv", "a");
    fputcsv($oneFile, $key);

    $twoFile = fopen("$databaseName/two.csv", "a");
    fputcsv($twoFile, $key);

    $threeFile = fopen("$databaseName/three.csv", "a");
    fputcsv($threeFile, $key);

    while (!feof($handle)) 
    {
      $myCsv = fgetcsv($handle); 
      if ($myCsv != false)
      {
        if ($myCsv[0] < 4)
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            $one[$oneIndex] = $myCsv[$i];
            $oneIndex++;
          }
          $oneFile = fopen("$databaseName/one.csv", "a");
          fputcsv($oneFile, $one);
          unset($one);
          fclose($oneFile);
        }
        else if ($myCsv[0] > 3 && $myCsv[0] < 7)
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            $two[$twoIndex] = $myCsv[$i];
            $twoIndex++;
          }
          $twoFile = fopen("$databaseName/two.csv", "a");
          fputcsv($twoFile, $two);
          unset($two);
          fclose($twoFile);
        }
        else if ($myCsv[0] > 6 && $myCsv[0]< 10)
        {
          for ($i = 0; $i < count($myCsv); $i++)
          {
            $three[$threeIndex] = $myCsv[$i];
            $threeIndex++;
          }
          $threeFile = fopen("$databaseName/three.csv", "a");
          fputcsv($threeFile, $three);
          unset($three);
          fclose($threeFile);
        }
      }
    }
    fclose($handle);
  }

  function searchIndex($databaseName, $tableName, $recordName, $id)
  {
    if (ord($id) > 96 && ord($id) < 106)
    {
      $this->getRecord($databaseName, "aToi", $recordName, $id);
    }

    else if (ord($id) > 105 && ord($id) < 115)
    {
      $this->getRecord($databaseName, "jTor", $recordName, $id);
    }

    else if (ord($id) > 114 && ord($id) < 123)
    {
      $this->getRecord($databaseName, "sToz", $recordName, $id);
    }
  }

  function searchId($databaseName, $tableName, $id)
  {
    if ($id < 4)
    {
      $this->retrieveRecord($databaseName, "one", $id);
    }

    else if ($id > 3 && $id < 7)
    {
      $this->retrieveRecord($databaseName, "two", $id);
    }

    else if ($id > 6 && $id < 10)
    {
      $this->retrieveRecord($databaseName, "three", $id);
    }
  }

  function join($databaseName, $tableName, $tabletoJoin, $recordName)
  {
    $handle = fopen("$databaseName/$tableName.csv", "r");
    
    $joinFile = fopen("$databaseName/joinFile.csv", "a");

    $key = fgetcsv($handle);
    $index = -1;

    $result = array();
    $found = false;

    for ($j = 0; $j < count($key); $j++)
    {
      if ($recordName == $key[$j]) 
      {
        $index = $j;
      }
    }

    while (!feof($handle)) 
    {
      $file = fopen("$databaseName/$tabletoJoin.csv", "r");
      $myCsv = fgetcsv($handle); 
      if ($myCsv != false)
      {
        while (!feof($file)) 
        {
          $myJoin = fgetcsv($file);
          if ($myCsv[$index] == $myJoin[0])
          {
            $result = array_merge($myCsv, $myJoin);
            fputcsv($joinFile, $result);
            unset($result);
          }
        }
      }
    }
    fclose($handle);
    fclose($joinFile);
    fclose($file);

    $joinFile = fopen("$databaseName/joinFile.csv", "r");
    while (!feof($joinFile)) 
    {
      $read = fgetcsv($joinFile); 
      if ($read != false)
      {
        for ($k = 0; $k < count($read); $k++)
        {
          if ($k != $index && $k != $index + 1)
          {
            echo $read[$k] . " ";
          }
        }
        echo "\n";
      }
    }
    fclose($joinFile);
    unlink("$databaseName/joinFile.csv");
  }
}