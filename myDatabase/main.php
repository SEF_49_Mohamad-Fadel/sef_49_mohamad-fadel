#!/usr/bin/php
<?php
require_once "DataBase.php";
require_once "Table.php";

$myDataBase = new DataBase();
$myTable = new Table();
$myDataBaseName = -1;
$myTableName = -1;

echo "Hello to Fadel's DataBase :)\n";

$input = readline("Enter sql statment or Q to exit: ");

while ($input != "Q")
{
	$line = explode(",", $input);

	//Selecting Database
	if ($line[0] == "USE" && $line[1] == "DATABASE")
	{
		$dabaseName = trim($line[2], "\"");
		if ($myDataBase->DbExists($dabaseName))
		{
			$myDataBaseName = $dabaseName;
			echo "Using " . $myDataBaseName . " Database\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
		else
		{
			echo "Sorry Database Not Found!\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Selecting Table in Database
	else if ($line[0] == "USE" && $line[1] == "TABLE")
	{
		if ($myDataBaseName != -1)
		{
			$tableName = trim($line[2], "\"");
			if ($myTable->TbExists($myDataBaseName, $tableName))
			{
				$myTableName = $tableName;
				echo "Using " . $myTableName . " Table\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Database to Use First (USE,DATABASE," .
						"\"databasename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//My Status
	else if ($line[0] == "STATUS")
	{
		if ($myDataBaseName != -1 && $myTableName != -1)
		{
			echo "Using " . $myDataBaseName . " DataBase\n";
			echo "Using " . $myTableName . " Table\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
		else
		{
			echo "No DataBase or Table Specified\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Creating Database
	else if ($line[0] == "CREATE" && $line[1] == "DATABASE")
	{
		$databaseName = trim($line[2], "\"");
		$myDataBase->createDatabase($databaseName);
		$input = readline("Enter sql statment or Q to exit: ");
	}

	//Creating Table in Database
	else if ($line[0] == "CREATE" && $line[1] == "TABLE")
	{
		if ($myDataBaseName != -1)
		{
			$tableName = trim($line[2], "\"");
			$columns = array();
			$j = 0;
			for ($i = 4; $i < count($line); $i++)
			{
				$columns[$j] = trim($line[$i], "\"");
				$j++;
			}

			$myTable->createTable($myDataBaseName, $tableName, $columns);

			$input = readline("Enter sql statment or Q to exit: ");
		}
		else
		{
			echo "Please Select Database to Use First (USE,DATABASE," .
					  "\"databasename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Deleting DataBase
	else if ($line[0] == "DELETE" && $line[1] == "DATABASE")
	{
		$databaseName = trim($line[2], "\"");
		if ($myDataBase->DbExists($databaseName))
		{
			$myDataBase->deleteDatabase($databaseName);
			$input = readline("Enter sql statment or Q to exit: ");
		}
		else
		{
			echo "Sorry Database Not Found!\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Deleting Table From DataBase
	else if ($line[0] == "DELETE" && $line[1] == "Table")
	{
		if ($myDataBaseName != -1)
		{
			$tableName = trim($line[2], "\"");
			if ($myTable->TbExists($myDataBaseName, $tableName))
			{
				$myTable->deleteTable($myDataBaseName, $tableName);
				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Database to Use First (USE,DATABASE," .
						"\"databasename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Adding Record To Table
	else if ($line[0] == "ADD" && $line[1] == "RECORD")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$data = array();
				$j = 0;
				for ($i = 2; $i < count($line); $i++)
				{
					$data[$j] = trim($line[$i], "\"");
					$j++;
				}
				$myTable->addRecord($myDataBaseName, $myTableName, $data);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Deleting Record From Table in Database
	else if ($line[0] == "DELETE" && $line[1] == "RECORD")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$id = trim($line[2], "\"");
				$myTable->deleteRecord($myDataBaseName, $myTableName, $id);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Retrieving Record From Table in Database as ID
	else if ($line[0] == "GET" && $line[1] == "RECORD")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$id = trim($line[2], "\"");
				$myTable->retrieveRecord($myDataBaseName, $myTableName, $id);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Retrieving Record From Table in Database as specific record name
	else if ($line[0] == "GET")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$index = trim($line[1], "\"");
				$id = trim($line[2], "\"");
				$myTable->getRecord($myDataBaseName, $myTableName, $index, $id);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Indexing a Record
	else if ($line[0] == "INDEX" && $line[1] == "RECORD")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$recordName = trim($line[2], "\"");
				$myTable->indexRecord($myDataBaseName, $myTableName, $recordName);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Indexing the id
	else if ($line[0] == "INDEX" && $line[1] == "ID")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$myTable->indexId($myDataBaseName, $myTableName);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Search a Record by Indexing
	else if ($line[0] == "INDEX" && $line[1] == "SEARCH")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$recordName = trim($line[2], "\"");
				$id = trim($line[3], "\"");
				$myTable->searchIndex($myDataBaseName, $myTableName, $recordName, $id);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Search an ID by Indexing
	else if ($line[0] == "ID" && $line[1] == "SEARCH")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$id = trim($line[2], "\"");
				$myTable->searchId($myDataBaseName, $myTableName, $id);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Join 2 tables
	else if ($line[0] == "JOIN")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$tabletoJoin = trim($line[1], "\"");
				$recordName = trim($line[2], "\"");
				$myTable->join($myDataBaseName, $myTableName, $tabletoJoin, $recordName);

				$input = readline("Enter sql statment or Q to exit: ");
			}
			else
			{
				echo "Sorry Table Not Found!\n";
				$input = readline("Enter sql statment or Q to exit: ");
			}
		}
		else
		{
			echo "Please Select Table to Use First (USE,TABLE," .
						"\"tablename\")\n";
			$input = readline("Enter sql statment or Q to exit: ");
		}
	}

	//Help
	else if ($line[0] == "H")
	{
		$myDataBase->help();
		$input = readline("Enter sql statment or Q to exit: ");
	}

	else
	{
		echo "Inproper Sql Statment :(\n";
		$input = readline("Enter sql statment or Q to exit: ");
	}
}
