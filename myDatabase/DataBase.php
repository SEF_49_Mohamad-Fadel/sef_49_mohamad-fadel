<?php

class DataBase
{
	protected $name;

	function __construct()
	{
		$this->name = "Fadel's DataBase";
	}

	function createDatabase($databaseName)
	{
		if (!is_dir($databaseName))
		{
			mkdir($databaseName);
			echo "Database is Created WoW!\n";
		}
		else
		{
			echo "Sorry database is created before :(\n";
		}
	}

	function deleteDatabase($dirPath) 
	{
	  if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') 
	  {
	    $dirPath .= '/';
	  }
	  $files = glob($dirPath . '*', GLOB_MARK);

	  foreach ($files as $file) 
	  {
	    if (is_dir($file)) 
	    {
	      self::deleteDir($file);
	    } 
	    else 
	    {
	      unlink($file);
	    }
	  }
	  rmdir($dirPath);
	  echo "Database is Deleted WoW\n";
	}

	function DbExists($databaseName)
	{
		if (is_dir($databaseName))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function Help()
	{
		echo "To Create New DataBase Please Type:\nCREATE,DATABASE,".
					"\"databasename\".\n";
		echo "To Create New Table Please Type:\nCREATE,TABLE,".
					"\"tableename\",COLUMNS,\"COLUMN1\",\"COLUMN2\"...\n";
		echo "To Delete DataBase Please Type:\nDELETE,DATABASE,".
					"\"databasename\".\n";
		echo "To Delete Table Please Type:\nDELETE,TABLE,".
					"\"tablename\".\n";
		echo "To ADD New Record Please Type:\nADD,RECORD,".
					"\"entry1\",\"entry2\",\"entry3\"...\n";
		echo "To Delete Record Please Type:\nDELETE,RECORD,".
					"\"id\".\n";															
	}
}