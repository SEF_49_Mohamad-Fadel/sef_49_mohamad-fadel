<?php 
require_once("libs/Controller.php");

class Hello extends Controller
{
	public function indexAction($id = 0) {
		$this->view->hello = 'hello world from Hello';
		$this->view->render('views/hello/index.phtml');
	}
}