<?php
require_once("../database/MySQLWrap.php");

class ServerController 
{
  public function wrapp($method, $request, $queryString) {
    $tableName = $request[0];
    $wrapper = new MySQLWrap();

    switch ($method) {
      case 'GET':
        $data = $request[1];

        if ($queryString) {
          $wrapper->selectQuery($method, $tableName, $queryString);
        }
        else {
          $wrapper->select($method, $tableName, $data);  
        }  
        break;

      case 'PUT':
        $dataUpdate = $request[1];
        $input = json_decode(file_get_contents('php://input'),true);
        $wrapper->update($method, $tableName, $dataUpdate, $input); 
        break;

      case 'POST':
        $input = json_decode(file_get_contents('php://input'),true);
        $wrapper->insert($method, $tableName, $input); 
        break;

      case 'DELETE':
        $toDelete = json_decode(file_get_contents('php://input'),true);
        $wrapper->delete($method, $tableName, $toDelete); 
        break;
    }
  }
}
