<?php 
//RewriteRule api/v1/(.+)$ index.php?rawInput=$1 [QSA,NC,L]
require_once("../controllers/ServerController.php");

// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$queryString = $_SERVER['QUERY_STRING'];

$server = new ServerController();
$server->wrapp($method, $request, $queryString);
?>  