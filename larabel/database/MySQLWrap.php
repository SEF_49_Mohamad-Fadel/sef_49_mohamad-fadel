<?php

class MySQLWrap
{
  public function DBConnection() {
    require_once("DBConnect.php");
    $conect = new DB_Connect();
    $con = $conect->connect();
    return $con;
  }

  public function select($method, $table, $data)
  {
    $con = $this->DBConnection();

    //api/index.php/film
    if (!$data) {
      $sql = "select * from `$table`";
    }

    //api/index.php/film/film_id={1,2,3,4}
    else {
      $data = explode("=", $data);
      $keyword = $data[0];
      $attributes = explode(',', trim($data[1],'{}'));
      $flag = "";

      for ($i = 0; $i < count($attributes); $i++) {
        $attribute = $attributes[$i];

        if ($flag != "or") {
            $flag = "or";
            $sql = "select * from `$table` WHERE $keyword = $attribute"; 
        }
        else {
          $sql .= " or $keyword = $attribute";
        }
      }
    }

    $result = mysqli_query($con,$sql);
    $this->display($con, $method, $data, $result);
    $con->close();
  }

  public function selectQuery($method, $table, $queryString)
  {
    $con = $this->DBConnection();

    $queryString = explode("&", $queryString);
    $flag = "";
    for ($i = 0; $i < count($queryString); $i++) {
      $qString = explode("=", $queryString[$i]);
      $keyword = $qString[0];
      $attributes = $qString[1];

      switch ($keyword) {
      //api/index.php/film?fields=title,description 
      case 'fields':
        echo  "field";
        $flag =  "fields";
        $sql = "select $attributes from `$table`";  
        break;

      case 'orderby':
        echo "order";
        //api/index.php/film?fields=title&orderby=title
        if ($flag == "fields") {
          $sql .= " order by $attributes"; 
          break;
        }
        //api/index.php/film?orderby=title
        else {
          $sql = "select * from `$table` order by $attributes";
          break;
        }

      //api/index.php/film?fields=title&orderby=title&sort=desc 
      case 'sort':
        echo "sort";
        $sql .= " $attributes"; 
        break;

      //api/index.php/film?search=%ace%  
      case 'search':
        echo "search";
        $searchedfield = $qString[1];
        $attributes = $qString[2];
        $sql = "select * from `$table` where $searchedfield like '$attributes'";   
        break;
      }
    }
    
    $result = mysqli_query($con,$sql);
    $this->display($con, $method, $queryString, $result);
    $con->close();
  }

  public function insert($method, $table, $input)
  {
    $con = $this->DBConnection();
    
    if ($table == "film") {
      require_once("models/Film.php");
      $film = new Film;
      $filmArray = $film->filmArray;

      $attribute = explode(",", $input);

      for($i = 0; $i < count($attribute); $i++) {
        $field = explode("=", $attribute[$i]);
        for ($j = 0; $j < count($filmArray); $j++) {
          if ($field[0] == $filmArray[$j]) {
            $filmArray[$j+1] = $field[1];
          }
        }
      }

      $film->filmArray = $filmArray;
      $result = $film->store();
      $this->display($con, $method, $film, $result);
    }
    else {
      //URL = http://localhost/api/index.php/film
      //BODY = "title=\"hahaha\",description=\"desc\",release_year=1,language_id=1"
      $sql = "INSERT INTO $table set $input";

      $result = mysqli_query($con,$sql);
      $this->display($con, $method, $input, $result);
      $con->close();
    }
  }

  public function update($method, $table, $dataUpdate, $input)
  {
    $con = $this->DBConnection();

    if ($table == "film") {
      require_once("models/Film.php");
      $film = new Film;

      $dataUpdate = explode("=", $dataUpdate);
      $field = $dataUpdate[0];
      $id = $dataUpdate[1];

      $filmUpdated = $film->update($id, $input);
      $this->display($con, $method, $filmUpdated, $result);
    }
  
    else {
      $dataUpdate = explode("=", $dataUpdate);
      $field = $dataUpdate[0];
      $id = $dataUpdate[1];

      //URL = http://localhost/api/index.php/film/film_id=1009
      //BODY = "title=\"lol\",description=\"no\",release_year=2001,language_id=4"
      $sql = "update $table set $input where $field = $id";

      $result = mysqli_query($con,$sql);
      $this->display($con, $method, $input, $result);
      $con->close();
    }
  }

  public function delete($method, $table, $toDelete)
  {
    $con = $this->DBConnection();

    if ($table == "film") {
      require_once("models/Film.php");
      $film = new Film;

      $data = explode("=", $toDelete);
      $field = $data[0];
      $id = $data[1];

      $filmDeleted = $film->delete($id);
      $this->display($con, $method, $filmDeleted, $result);
    }
  
    else {
      $data = explode("=", $toDelete);
      $field = $data[0];
      $id = $data[1];
      
      //URL = http://localhost/api/index.php/film/film_id=1011
      //BODY = "film_id=1011"
      $sql = "delete from $table where $field = $id";

      $result = mysqli_query($con,$sql);
      $this->display($con, $method, $input, $result);
      $con->close();
    }
  }

  public function display($con, $method, $data, $result) 
  {
    if ($method == 'GET') {
      if (mysqli_num_rows($result) == 0) {
        echo "Sorry no results !!";
      }
      else {
        if (!$data) {
          echo '[';
        }
        for ($i = 0; $i < mysqli_num_rows($result); $i++) {
          echo ($i > 0?',':'').json_encode(mysqli_fetch_object($result));
        }
        if (!$data) echo ']';
      }
    } 
    elseif ($method == 'POST') {
      $data->display();
    } 

    elseif ($method == 'PUT' || $method == "DELETE") {
      echo json_encode($data);
    }
  }
}