<?php  

class User {
	public $name;
	public $age;

	public function __construct() {
		$this->name = "M and M";
		$this->age = 24;
	}
	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getAge() {
		return $this->age;
	}

	public function setAge($age) {
		$this->age = $age;
	}
}