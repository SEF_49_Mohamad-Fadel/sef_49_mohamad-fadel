<?php 

class Bootstrap {
	public function __construct() {
		//router
		$tokens = explode('/', rtrim($_SERVER['REQUEST_URI'],'/'));

		if (isset($tokens[2])) {
			$controllerName = ucfirst($tokens[2]);
			if (file_exists("controllers/".$controllerName.".php")) {
				require_once("controllers/".$controllerName.".php");
				$controller = new $controllerName;

				if (isset($tokens[3])) {
					$actionName = $tokens[3] . "Action";

					if (isset($tokens[4])) {
						$controller->{$actionName}($tokens[4]);
					}
					else {
						$controller->{$actionName}();
					}
				}
				else {
					//default action
					$controller->IndexAction();
				}
			}
			else {
				require_once("controllers/Error.php");
				$controllerName = 'Error';
				$controller = new $controllerName;
				$controller->IndexAction();
			}
		}
		else {
			require_once("controllers/Index.php");
			$controllerName = 'index';
			$controller = new $controllerName;
			$actionName = "indexAction";
			$controller->{$actionName}();
		}
	}
}

