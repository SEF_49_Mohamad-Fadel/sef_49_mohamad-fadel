<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>TO DO</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/main.css">
  </head>

  <body>
    <div id="container">
    
      <div class="section">
        <h1>SEF ToDo List</h1>

        <div class="content-top">
          <h2>Item</h2>

          <input id="task" placeholder = "Subject" required="required">
          <button id="add">Add</button>
          <textarea id="do" placeholder = "Message" required="required"></textarea>
          <span class="separator"></span>

        </div>
        
  <script src="js/editFunction.js"></script>  
  </body>     
</html>
