<?php
require_once "GameGenerator.php";
class GameSolver
{
  protected $opList;
  protected $numList;
  protected $number;

  function __construct(GameGenerator $gG)
  {
    $this->opList = array("s", "+", "-", "*", "/");
    $this->numList = $gG->generateList();
    $this->number = $gG->generateNum();
  } 

  function permute($items, $perms = array()) 
  {
    if (empty($items)) 
    {
      $return = array($perms);
    }  
    else 
    {
      $return = array();
      for ($i = count($items) - 1; $i >= 0; --$i) 
      {
        $newitems = $items;
        $newperms = $perms;
        list($foo) = array_splice($newitems, $i, 1);
        array_unshift($newperms, $foo);
        $return = array_merge($return, $this->permute($newitems, $newperms));
      }
    }
    return $return;
  }

  function combination($chars, $size, $combo = array()) 
  {
    # if it's the first iteration, the first set 
    # of combinations is the same as the set of characters
    if (empty($combo)) 
    {
      $combo = $chars;
    }

    # we're done if we're at size 1
    if ($size == 1) 
    {
      return $combo;
    }
    # initialise array to put new values in
    $new_combinations = array();

    # loop through existing combinations and character set to create strings
    foreach ($combo as $combo) 
    {
      foreach ($chars as $char) 
      {
        $new_combinations[] = $combo . $char;
      }
    }
    # call same function again for the next iteration
    return $this->combination($chars, $size - 1, $new_combinations);
  }

  function search($arr, $number)
  {
    for ($i = 0; $i < count($arr); $i++)
    {
      if ($arr[$i] == $number)
        return true;
    }
    return false;
  }


  function solveList()
  {
    print_r($this->numList);
    print_r($this->opList);
    echo "Number to find is" . "\n" . "$this->number" . "\n";

    $numPermutated = $this->permute($this->numList);
    $opPermutated = $this->combination($this->opList, 6);

    $stringEnd = array();
    $number = -1;
    $numbers = array();
    $expressions = array();
    $counter = 0;
    $counter2 = 0;
    $expression = " ";
    $tempoExp = array();

    for ($i = 0; $i < count($numPermutated); $i++)
    {
      for ($j = 0; $j < count($opPermutated); $j++)
      {
        $stringEnd[$i][$j] = $numPermutated[$i][0] . $opPermutated[$j][0];
        for ($k = 1; $k < 6; $k++)
        {
          $stringEnd[$i][$j] = $stringEnd[$i][$j] . $numPermutated[$i][$k] . $opPermutated[$j][$k];
          $temp[$i][$j] = $stringEnd[$i][$j];
        }
        
        if(strpos($temp[$i][$j], "s") != false)
        {
          $tempoExpA = explode("s", $temp[$i][$j]);
          $tempoExp = $tempoExpA[0];
          $stringEval[$i][$j] = eval('return ' . $tempoExp . ';');
        }
        else
        {
          $tempoExp = substr($temp[$i][$j], 0, strlen($temp[$i][$j]) - 1);
          $stringEval[$i][$j] = eval('return ' . $tempoExp . ';');
        }
        

        if ($stringEval[$i][$j] == $this->number)
        {
          $number = $stringEval[$i][$j];
          $expression = $tempoExp;
          break;
        }

        else if (($stringEval[$i][$j] > $this->number - 2) && ($stringEval[$i][$j] < $this->number + 2) && is_int($stringEval[$i][$j]))
        {
          if (!$this->search($numbers, $stringEval[$i][$j]))
          {
             $numbers[$counter] = $stringEval[$i][$j];
             $expressions[$counter2] = $tempoExp;
             $counter++;
             $counter2++;
          }
        }
      }
    }

    if ($number == $this->number)
    {
      echo "the exact solution is:\n";
      echo $number . "\n";
      echo "the expression for exact solution is:\n";
      echo $expression . "\n";
    }
    else
    {
      echo "Sorry no exact solution found:\n";
      print_r($numbers);
      echo "It's expression is:\n";
      print_r($expressions);
    }
  }
}