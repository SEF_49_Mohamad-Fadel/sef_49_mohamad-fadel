
@extends('main.home')

@section('content')
<link rel="stylesheet" href="../css/app.css">
<style>
  html,body{font:normal 0.9em arial,helvetica;}
  #log {width:440px; height:200px; border:1px solid #7F9DB9; overflow:auto;}
  #msg {width:330px;}
</style>

<script>
  var socket;

  function createSocket(host) {

    if ('WebSocket' in window)
        return new WebSocket(host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(host);

    throw new Error("No web socket support in browser!");
  }

  function init() {
    var host = "ws://localhost:12345/chat";
    try {
      socket = createSocket(host);
      //log('WebSocket - status ' + socket.readyState);
      socket.onopen = function(msg) {
          //log("Welcome - status " + this.readyState);
      };

      socket.onmessage = function(msg) {
          log(msg.data);
      };
      socket.onclose = function(msg) {
          log("Disconnected - status " + this.readyState);
      };
    }
    catch (ex) {
        log(ex);
    }
    document.getElementById("msg").focus();
  }

  function send(data, channelid) {
    var msg = document.getElementById('msg').value;
    var name = data['name'];
    var id = data['id']
    msg += '-' + id + '-' + name + '-' + channelid;

    try {
        socket.send(msg);
    } catch (ex) {
        log(ex);
    }
  }
  function quit() {
    log("Goodbye!");
    socket.close();
    socket = null;
  }

  function log(msg) {
    document.getElementById("log").innerHTML += "<br>" + msg;
  }
  function onkey(event) {
    if (event.keyCode == 13) {
        send();
    }
  }
</script>
<body onload="init()">
        <!--chat_area-->
        <div id="log" class="message_write" style="width:800px; height: 500px;margin-left:20px;">
          @foreach($messages as $message)
          <th>USER: {{ $message->user()->get()[0]->name }}</th>
          <p>Message: {{ $message->text }}</p>
          @endforeach
        </div>
        
        </div></div>
        <div style="margin: 0 150px 0 420px;">   
          <br/><input id="msg" type="text" onkeypress="onkey(event)" class="form-control" placeholder="type a message"/>
          <br/><button onclick="send({{ Auth::user() }}, {{ $cid }})" class="pull-left btn btn-success">Send</button>
          <button onclick="quit()" class="pull-right btn btn-danger">Quit</button>
        </div>
  @endsection
 