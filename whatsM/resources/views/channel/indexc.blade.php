<html>
<head>
<title>Simple Chat</title>
<link rel="stylesheet" href="css/app.css">
<style>
  html,body{font:normal 0.9em arial,helvetica;}
  #log {width:440px; height:200px; border:1px solid #7F9DB9; overflow:auto;}
  #msg {width:330px;}
</style>

<script>
  var socket;

  function createSocket(host) {

    if ('WebSocket' in window)
        return new WebSocket(host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(host);

    throw new Error("No web socket support in browser!");
  }

  function init() {
    var host = "ws://localhost:12345/chat";
    try {
      socket = createSocket(host);
      //log('WebSocket - status ' + socket.readyState);
      socket.onopen = function(msg) {
          //log("Welcome - status " + this.readyState);
      };
      socket.onmessage = function(msg) {
          log(msg.data);
      };
      socket.onclose = function(msg) {
          log("Disconnected - status " + this.readyState);
      };
    }
    catch (ex) {
        log(ex);
    }
    document.getElementById("msg").focus();
  }

  function send(id) {
    var msg = document.getElementById('msg').value;
    msg += '-' + id;

    try {
        socket.send(msg);
    } catch (ex) {
        log(ex);
    }
  }
  function quit() {
    log("Goodbye!");
    socket.close();
    socket = null;
  }

  function log(msg) {
    document.getElementById("log").innerHTML += "<br>" + msg;
  }
  function onkey(event) {
    if (event.keyCode == 13) {
        send();
    }
  }
</script>

</head>
<body onload="init()">
<link rel="stylesheet" href="app.css">
<div class="main_section">
  <div class="container">
    <div class="chat_container">

      <div class="col-sm-3 chat_sidebar">
        <div class="row">
          <div id="custom-search-input">
            <div class="input-group col-md-12">
              <input type="text" class="  search-query form-control" placeholder="Conversation" />
              <button class="btn btn-danger" type="button">
              <span class=" glyphicon glyphicon-search"></span>
              </button>
            </div>
          </div>

          <div class="dropdown all_conversation">
            <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-weixin" aria-hidden="true"></i>
             All Channels
            <span class="caret pull-right"></span>
            </button>
          </div>

          <div class="member_list">
            <ul class="list-unstyled">
              <li class="left clearfix">
                <span class="chat-img pull-left">
                <img src="" alt="User Avatar" class="img-circle">
                </span>
                <div class="chat-body clearfix">
                  <div class="header_sec">
                    <a href="{{ route('channels.show', ['cid'=>'1']) }}"><strong class="primary-font">Channel 1</strong></a>
                  </div>
                </div>
              </li>
              <li class="left clearfix">
                <span class="chat-img pull-left">
                <img src="" alt="User Avatar" class="img-circle">
                </span>
                <div class="chat-body clearfix">
                  <div class="header_sec">
                    <a href="{{ route('channels.show', ['cid'=>'2']) }}"><strong class="primary-font">channel 2</strong> </a>
                  </div>
                </div>
              </li>
              <li class="left clearfix">
                <span class="chat-img pull-left">
                <img src="" alt="User Avatar" class="img-circle">
                </span>
                <div class="chat-body clearfix">
                  <div class="header_sec">
                    <a href="{{ route('channels.show', ['cid'=>'3']) }}"><strong class="primary-font">Channel 3</strong> </a>
                  </div>
                </div>
              </li>
            </ul>
          </div>

        </div>
      </div>


      <!--chat_sidebar-->
      <div class="col-sm-9 message_section">
        <div class="row">
          <div class="new_message_head">
            <div class="pull-right">
              <div class="dropdown">
                <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-cogs" aria-hidden="true"></i>  Setting
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Profile</a></li>
                  <li><a href="#">Logout</a></li>
                </ul>
              </div>
            </div>
          </div>

        <!--chat_area-->
        <div id="log" class="message_write" style="width:800px; height: 500px;margin-left:20px;">
          @foreach($messages as $message)
          <th>USER: {{ $message->user()->get()[0]->name }}</th>
          <p>Message: {{ $message->text }}</p>
          @endforeach
        </div>
        

        </div>
        <br/><input id="msg" type="text" onkeypress="onkey(event)" class="form-control" placeholder="type a message"/>
        <br/><button onclick="send({{ Auth::id() }})" class="pull-left btn btn-success">Send</button>
        <button onclick="quit()" class="pull-right btn btn-danger">Quit</button>

        </div>
      </div>
    </div> 
  </div>
</div>
</body>

  
 