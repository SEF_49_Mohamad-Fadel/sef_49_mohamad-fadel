
<html>
<head>
<title>Simple Chat</title>
<script src="https://use.fontawesome.com/45e03a14ce.js"></script>
<link rel="stylesheet" href="css/app.css">
  <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>

<body>
<div class="main_section">
  <div class="container">
  
      <div class="loginBox nav navbar-nav pull-right">
        @if (Auth::guest())
        <li><a href="login">Login</a></li>
        <li><a href="register">Register</a></li>
      </div>

      <h3>WhatsM Chatting APP</h3>
      <img src="wallchat.jpg" width="1000" height="600">
      @else

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
          <li>
            <a href="logout" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Logout</a>

            <form id="logout-form" action="logout" method="POST" style="display: none;">
            {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </li>
    </div>

    <div class="chat_container">
      <div class="col-sm-3 chat_sidebar">
        <div class="row">
          <div id="custom-search-input">
            <div class="input-group col-md-12">
              <input type="text" class="  search-query form-control" placeholder="Conversation" />
              <button class="btn btn-danger" type="button">
              <span class=" glyphicon glyphicon-search"></span>
              </button>
            </div>
          </div>

          <div class="dropdown all_conversation">
            <button type="button">
            <i class="fa fa-weixin" aria-hidden="true"></i>
             All Channels
            <span class="caret pull-right"></span>
            </button>
          </div>

          <div class="member_list">
          @foreach($channels as $channel)
            <ul class="list-unstyled">
              <li class="left clearfix">
                <span class="chat-img pull-left">
                <img src="chanel.jpg" alt="User Avatar" class="img-circle">
                </span>
                <div class="chat-body clearfix">
                  <div class="header_sec">
                    <a href="{{ route('channels.show', ['cid'=>$channel->id]) }}"><strong class="primary-font">{{ $channel->name }}</strong></a>
                  </div>
                </div>
              </li>
            </ul>
            @endforeach
          </div>
        </div>
      </div>
@endif
    </div> 

     <div>
      @yield('content')
    </div>

  </div>
</div>
</body>
</html>

  
 