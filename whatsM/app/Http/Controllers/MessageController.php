<?php

namespace whatsm\Http\Controllers;

use whatsm\Message;
use Illuminate\Http\Request;
//use Auth;
class MessageController extends Controller
{

  public function __construct(Message $messages)
  {
    $this->messages = $messages;
  }    

  /**
    * @param ChatRoom $chatRoom
    * @return mixed
    */
  public function getByChatRoom(Channel $chatRoom)
  {
    return $chatRoom->messages;
  }    

  /**
    * @param ChatRoom $chatRoom
    * @return static
    */
  public function createInChatRoom(Channel $chatRoom)
  {
    $message = $this->messages->newInstance(Input::all());        
    $message->chatRoom()->associate($chatRoom);
    $message->user()->associate($this->me());        
    $message->save();        
    return $message;
  }    

  /**
    * @param $lastMessageId
    * @param ChatRoom $chatRoom
    * @return mixed
    */
  public function getUpdates($lastMessageId, Channel $chatRoom)
  {
    return $this->messages->byChatRoom($chatRoom)->afterId($lastMessageId)->get();
  }  

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  { 
    //$id = (Auth::id());
    $message = new Message();
    $message->text = $request->sd;
    $message->channel_id = $request->cid;
    $message->user_id = $request->id;
    $message->save();
    return $request->sd;
  }

  /**
   * Display the specified resource.
   *
   * @param  \whatsm\Message  $message
   * @return \Illuminate\Http\Response
   */
  public function show(Message $message)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \whatsm\Message  $message
   * @return \Illuminate\Http\Response
   */
  public function edit(Message $message)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \whatsm\Message  $message
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Message $message)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \whatsm\Message  $message
   * @return \Illuminate\Http\Response
   */
  public function destroy(Message $message)
  {
      //
  }
}
