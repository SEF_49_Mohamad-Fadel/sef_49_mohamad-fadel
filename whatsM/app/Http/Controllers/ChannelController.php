<?php

namespace whatsm\Http\Controllers;

use whatsm\Channel;
use whatsm\Message;
use Illuminate\Http\Request;
use Auth;

class ChannelController extends Controller
{


  /** 
  *@return \Illuminate\Http\Response          
  * @param ChatRoom $chatRooms
  */
  public function __construct(Channel $chatRooms)
  {
    $this->channels = $chatRooms;
  }    

  /**
    * @return \Illuminate\Database\Eloquent\Collection|static[]
    */
   public function getAll()
   {
      return $this->channels->all();
   }    

   /**
    * @param ChatRoom $chatRoom
    * @return ChatRoom
    */
   public function show($cid)
   {
      $messages = Message::all()->where('channel_id',$cid);
      $channels = Channel::all();

      $data = array(
      'channels'=>$channels,
      'messages'=>$messages,
      'cid'=> $cid
      );
    return view('channel.index', $data);
   }    

   /**
    * @return static
    */
   public function create()
   {
      return $this->channels->create(Input::all());
   }    


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $messages = Message::all();
    $channels = Channel::all();

    $data = array(
      'channels'=>$channels,
      'messages'=>$messages
      );
    return view('main.home', $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \whatsm\Channel  $channel
   * @return \Illuminate\Http\Response
   */
  public function edit(Channel $channel)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \whatsm\Channel  $channel
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Channel $channel)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \whatsm\Channel  $channel
   * @return \Illuminate\Http\Response
   */
  public function destroy(Channel $channel)
  {
      //
  }
}
