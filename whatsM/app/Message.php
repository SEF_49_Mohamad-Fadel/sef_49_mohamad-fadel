<?php

namespace whatsm;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  /**
    * @var string
    */
   protected $table = 'messages';    

   /**
    * @var array
    */
   protected $fillable = array('text');    

   /**
    * @var array
    */
   protected $with = array('user');    

   /**
    * @param $query
    * @param $lastId
    * @return mixed
    */
   public function scopeAfterId($query, $lastId)
   {
      return $query->where('id', '>', $lastId);
   }    

   /**
    * @param $query
    * @param $chatRoom
    * @return mixed
    */
   public function scopeByChatRoom($query, $chatRoom)
   {
       return $query->where('channel_id', $chatRoom->id);
   }    

   /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function chatRoom()
   {
       return $this->belongsTo('Channel', 'channel_id');
   }    

   /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function user()
   {
       return $this->belongsTo(User::class, 'user_id');
   }
}
