<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::resource('/','ChannelController');

Route::resource('messages','MessageController');
Route::resource('channels','ChannelController');
Route::resource('users','UserController');

/*
// Channel rooms
Route::get('/channels', array('uses' => 'ChannelController@getAll'));
Route::get('/channels/{Channel}', array('uses' => 'ChannelController@show'));
Route::post('/channels', array('uses' => 'ChannelController@create'));

// Messages
Route::get('/messages/{Channel}', array('uses' => 'MessageController@getByChatRoom'));
Route::post('/messages/{Channel}', array('uses' => 'MessageController@createInChatRoom'));
Route::get('/messages/{lastMessageId}/{Channel}', array('uses' => 'MessageController@getUpdates'));

// Users
Route::get('/users/login/kareem', array('uses' => 'UserController@loginKareem'));
Route::get('/users/login/mohamed', array('uses' => 'UserController@loginMohamed'));
Route::model('Channel', 'Channel');

*/